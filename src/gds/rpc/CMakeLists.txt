CREATE_RPC_TARGET(rawgapi)
target_include_directories(rawgapi_rpc PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/../awgtpman)

CREATE_RPC_TARGET(rtestpoint)
target_include_directories(rtestpoint_rpc PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/../awgtpman)
