/* Version: $Id$ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rmorg							*/
/*                                                         		*/
/* Module Description: 	Reflective memory organization			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Sep98   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gds html pages					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_RMORG_H
#define _GDS_RMORG_H

#ifdef __cplusplus
extern "C" {
#endif


/* Header File List: */
#include "dtt/hardware.h"
#include "map.h"

/** @name Reflective Memory Organization
    This module defines the organizational structure of the reflective
    memory. This API is a wrapper around the "map.h" file of the DAQ 
    system.

    @memo Defines the memory layout
    @author Written September 1998 by Daniel Sigg
    @see DAQ reflective memory organization, Testpoint Definition
    @version 0.1
************************************************************************/

/*@{*/

/** @name Constants and flags.
    Constants and flags of the reflective memory layout.

    @memo Constants and flags
    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

/** @name Limits

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

/** Size of test point channel data element. This number is in bytes.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DATUM_LEN		sizeof (float)

/** Maximum number of test point nodes. This is identical to the number
    of reflective memory rings. UNIX target has command line argument
    to set the node id.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_MAX_NODE        256

/** Maximum number of test point interfaces. This number is currently 4
    (LSC/ASC excitation and LSC/ASC test point readout).

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_MAX_INTERFACE	2


/** Highest test point index number.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_HIGHEST_INDEX	60000

/** Maximum number of preselected testpoints.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_MAX_PRESELECT	20

/*@}*/

/** @name Identification numbers

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

/** Test point node id of node 0.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_NODE_0		0

/** Test point node id of node 1.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_NODE_1		1

/** Excitation test point interface id

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_EX_INTERFACE	0


/** Test point interface id

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_TP_INTERFACE	1


/** Test point interface id of the diagital-to-analog converter. (Does 
    not have a corresponding reflective memory region; this is a logical
    id only.)

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DAC_INTERFACE	100

/** Test point interface id of DS340 signal generator. (Does not have
    a corresponding reflective memory region; this is a logical id only.)

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DS340_INTERFACE	101

/*@}*/

/** @name Index lengths

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

/*@}*/

/** @name Channel lengths

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

/** Channel length of LSC test points. This is 1/16th of the LSC sampling
    rate of 16386Hz.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_CHN_LEN		(model_rate_Hz / NUMBER_OF_EPOCHS)


/*@}*/

/** @name Test point numbers

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/


/** Defines the test point ID offset for the LSC system.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_LSC_TP_OFS	10000

/** Defines the test point ID offset for the ASC excitation engine.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_ASC_EX_OFS	20000

/** Defines the test point ID offset for the ASC system.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_ASC_TP_OFS	30000

/** Defines the test point ID offset for the DAC channels.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_DAC_OFS		40000

/** Defines the test point ID offset for the DS340 channels.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_DS340_OFS		50000

/** Defines the test point ID offset for the last TP.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_END_OFS		60000

/** Defines the test point ID for clearing all selected TPs.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define _TP_CLEAR_ALL		((unsigned short)(-1))

/*@}*/
/*@}*/

/** @name Functions and Macros.
    * Functions of the reflective memory layout.

    @memo Functions and Macros
    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/*@{*/

#define TP_NODE_INTERFACE_TO_UNIT_ID(B,A) ( \
        ((A) == TP_EX_INTERFACE ? \
                ((B) == TP_NODE_0 ? GDS_4k_LSC_EX_ID : \
                ((B) == TP_NODE_1 ? GDS_2k_LSC_EX_ID : \
                 -1)) :     \
        ((A) == TP_TP_INTERFACE ? \
                ((B) == TP_NODE_0 ? GDS_4k_LSC_TP_ID : \
                ((B) == TP_NODE_1 ? GDS_2k_LSC_TP_ID : \
                 -1)) :     \
        -1)))


/* Macro assumes ETMX SUS controllers, VEA ICS115s and HEPI controllers are on the second RFM net */

#define TP_ID_TO_INTERFACE(A) ( \
	((A) == 0 ? -1 :	    \
	((A) < TP_ID_LSC_TP_OFS ? TP_EX_INTERFACE :	    \
	((A) < TP_ID_ASC_EX_OFS ? TP_TP_INTERFACE :	    \
	((A) < TP_ID_ASC_TP_OFS ? TP_EX_INTERFACE :	    \
	((A) < TP_ID_DAC_OFS ? TP_TP_INTERFACE :	    \
	((A) < TP_ID_DS340_OFS ? TP_DAC_INTERFACE :	    \
        ((A) < TP_ID_END_OFS ? TP_DS340_INTERFACE :         \
         -1))))))))

#define TP_NODE_ID_TO_RFM_ID(A) 0


#define IS_TP(A) \
        (A->tpNum <= 0 ? 0 : 1)

#define IS_UNIT(A) \
        (UNIT_ID_TO_RFM_NODE_ID(A) == -1 ? 0 : 1)


#define UNIT_ID_TO_DATA_OFFSET(A) (IS_UNIT(A) ? DATA_OFFSET_DCU(A): -1)
#define UNIT_ID_TO_DATA_BLOCKSIZE(A) \
	(IS_UNIT(A) ? (UNIT_ID_TO_RFM_SIZE(A) / DATA_BLOCKS): 0)


/*@}*/

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_GDS_RMORG_H */
