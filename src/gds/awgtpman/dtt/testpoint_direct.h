/* Version: $Id$ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testpoint						*/
/*                                                         		*/
/* Module Description: API for handling testpoints			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 25June98 D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testpoint.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TESTPOINT_H
#define _GDS_TESTPOINT_H

#ifdef __cplusplus
extern "C" {
#endif


/* Header File List: */
#include "tconv.h"
#include "testpoint_structs.h"


/**
   @name Test Point API
   A set of routines to handle digital test points in reflective
   memory. These routines can be called from any machine and will
   use rpc to commmunicate with the reflective memory system 
   controller when necessary. Test points are always valid for full
   one second intervals and can not be changed in between. Test 
   points are used to read data from LSC and ASC, as well as writing
   waveforms to them through the excitation engine.

   @memo API to digital test points in the reflective memory
   @author Written June 1998 by Daniel Sigg
   @version 0.1
************************************************************************/

/*@{*/

/** @name Constants and flags.
    * Constants and flags of the test point API.

    @memo Constants and flags
    @author DS, June 98
    @see Test point API
************************************************************************/

/*@{*/


/** Defines the epoch when test points are getting updated.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TESTPOINT_UPDATE_EPOCH	8

/** Defines the first epoch when testpoint information is valid.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TESTPOINT_VALID1	10

/** Defines the last epoch when testpoint information is valid..

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TESTPOINT_VALID2	4

/*@}*/


/** @name Data types.
    * Data types of the test point API.

    @memo Data types
    @author DS, June 98
    @see Test point API
************************************************************************/

/*@{*/



/*@}*/

/** @name Functions.
    * Functions of the test point API.

    @memo Functions
    @author DS, June 98
    @see Test point API
************************************************************************/

/*@{*/




/** Gets the test point index directly. This routine is similar to
    tpQuery, however, it will only try to read the test point index 
    locally; and return with an error if the index is not accessible.

    @param node test point node
    @param tpinterface testpoint interface id
    @param tp list of test point ID numbers
    @param tplen length of test point list
    @param time time of query request
    @param epoch epoch of query request
    @return Number of read test points if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
   int tpGetIndexDirect (int node, int tpinterface, testpoint_t tp[], 
   int tplen, taisec_t time, int epoch);


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: testpoint_direct				*/
/*                                                         		*/
/* Procedure Description: opens interface to direct testpoint control   */
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_direct (void);

/*@}*/

/*@}*/


#ifdef __cplusplus
}
#endif

#endif /*_GDS_TESTPOINT_H */
