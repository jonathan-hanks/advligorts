//
// Created by erik.vonreis on 5/21/21.
//

#ifndef DAQD_TRUNK_TESTPOINT_STRUCTS_H
#define DAQD_TRUNK_TESTPOINT_STRUCTS_H

/** Defines the type of a test point.

    @author DS, June 98
    @see Test point API
************************************************************************/
typedef unsigned int testpoint_t;



#endif // DAQD_TRUNK_TESTPOINT_STRUCTS_H
