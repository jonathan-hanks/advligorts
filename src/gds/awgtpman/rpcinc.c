static char *versionId = "Version $Id$" ;
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rpcinc							*/
/*                                                         		*/
/* Module Description: implements functions used by the gds rpc 	*/
/* interface								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <syslog.h>
#include "sockutil.h"

#include "dtt/gdsutil.h"
#define _RPC_HDR
#define _RPC_XDR
#include "dtt/gdstask.h"
#include "dtt/rpcinc.h"


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _NETID		  net protocol used for rpc		*/
/*            _SHUTDOWN_DELAY	  wait period before shut down		*/
/*            SIG_PF		  signal function prototype 		*/
/*            _IDLE		  server idle state			*/
/*            _BUSY		  server busy state			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _NETID			"tcp"
#define _SHUTDOWN_DELAY		120		/* 120 s */
#ifndef SIG_PF
#define	SIG_PF 			void (*) (int)
#endif
#define	_IDLE 			0
#define	_BUSY	 		1
#define _INIT_SLEEP		1000000UL	/*   1 ms */


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: rpcCBService_t	rpc service task argument		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   struct rpcCBService_t {
      u_long* 		prognum;
      u_long 		progver;
      SVCXPRT** 	transport;
      rpc_dispatch_t	dispatch;
   };
   typedef struct rpcCBService_t rpcCBService_t;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: servermux		protects globals			*/
/*          serverState		state of server		 		*/
/*          shutdown		shutdown flag for port monitor serveres	*/
/*          runsvc		1 if rpc svc already runnning		*/
/*          defaultServerMode	the default server mode			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static mutexID_t		_servermux;
   static int			_serverState = _IDLE;
   static int*			_shutdown;

   static int			runsvc = 0;
   FILE*		gdslog;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetHostaddress				*/
/*                                                         		*/
/* Procedure Description: returns a host address given the host name	*/
/*                                                         		*/
/* Procedure Arguments: hostname, inet address (return value)		*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcGetHostaddress (const char* hostname, struct in_addr* addr)
   {
      if (hostname == NULL) {
         addr->s_addr = inet_addr ("127.0.0.1");
         return 0;
      }
   
      /* determine host address */
      /* use DNS if necessary */
      return nslookup (hostname, addr);

   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetLocaladdress				*/
/*                                                         		*/
/* Procedure Description: returns the local address 			*/
/*                                                         		*/
/* Procedure Arguments: inet address (return value)			*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcGetLocaladdress (struct in_addr* addr)
   {  
      char		myname[256];	/* local host name */
   
      if ((gethostname (myname, sizeof (myname)) != 0) ||
         (rpcGetHostaddress (myname, addr) != 0)) {
         return -1;
      }
      else {
         return 0;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetClientaddress				*/
/*                                                         		*/
/* Procedure Description: returns a client address of the rpc call	*/
/*                                                         		*/
/* Procedure Arguments: service handle, inet address (return value)	*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcGetClientaddress (const SVCXPRT* xprt,
                     struct in_addr* clntaddr)
   {
      /* determine network address client address */
      {
         struct sockaddr_in*	addr;
         addr = svc_getcaller ((SVCXPRT*) xprt);
         *clntaddr = addr->sin_addr;
      }

      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: checkStdInHandle				*/
/*                                                         		*/
/* Procedure Description: check FD 0 to determine if it was called by	*/
/*			  a port monitor				*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 1 if called by port mon., 0 if not, -1 if failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static int checkStdInHandle (void)
   {
      struct sockaddr_in saddr;
      int 		asize = sizeof (saddr);
      int 		rpcfdtype;
      int 		ssize = sizeof (int);
   
      /* get the socket address of FD 0 */
      if (getsockname (0, (struct sockaddr*) &saddr, &asize) == 0) {
      
         /* check whether valid internet address */
         if (saddr.sin_family != AF_INET) {
            return -1;
         }
      	 /* get socket type: tcp only */
         if ((getsockopt (0, SOL_SOCKET, SO_TYPE,
            (char*) &rpcfdtype, &ssize) == -1) || 
            (rpcfdtype != SOCK_STREAM)) {
            return -1;
         }
         return 1;
      }
      else {
         return 0;
      }
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcInitializeServer				*/
/*                                                         		*/
/* Procedure Description: initializes rpc server stuff			*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - pointer to port monitor boolean	*/
/*                      svc_fg - whether to start service in foreground	*/
/*                      svc_mode - sets rpc service mt mode		*/
/*                      transp - returned transport handle		*/
/*                      proto - returned protocol type			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcInitializeServer (int* rpcpmstart, int svc_fg, int svc_mode,
                     SVCXPRT** transp, int* proto) 
   {
      /* ignore pipe signals */
   #if _POSIX_SOURCE
      struct sigaction action;
      action.sa_flags = 0;
      sigemptyset (&action.sa_mask);
      action.sa_handler = SIG_IGN;
      sigaction (SIGPIPE, &action, NULL);
   #else
      (void) sigset (SIGPIPE, SIG_IGN);
   #endif
   

      /* find out whether started by port monitor */
      *rpcpmstart = checkStdInHandle ();
      if (*rpcpmstart == -1) {
         return -3;
      }

      /* fork on UNIX machines */
      if ((*rpcpmstart == 0) && (svc_fg == 0)) {
         int 		size;
         struct rlimit	rl;
         int 		pid;		/* process id */
         int		i;
      
         /* try to fork */
         pid = fork ();
         if (pid < 0) {
            /* error */
            gdsError (GDS_ERR_PROG, "cannot fork");
            return -4;
         }
      	 /* exit if parent */
         if (pid != 0) {
            exit (0);
         }
      	 /* child process starte here: close all unnecessary files */
         rl.rlim_max = 0;
         getrlimit (RLIMIT_NOFILE, &rl);
         if ((size = rl.rlim_max) == 0) {
            gdsError (GDS_ERR_PROG, "unable to close file handles");
            return -5;
         }
         for (i = 0; i < size; i++) {
            (void) close(i);
         }
         i = open("/dev/null", 2);
         (void) dup2(i, 1);
         (void) dup2(i, 2);
         setsid ();
         openlog ("gdsrsched", LOG_PID, LOG_DAEMON);
         gdsDebug ("server process has forked");
      }

       /* create transport */
      /* port map service */
      {
         int 		sock;		/* socket handle */
   
         /* create socket if not portmap */
         if (*rpcpmstart == 1) {
            sock = 0;
            *proto = 0;
         }
         else {
            sock = RPC_ANYSOCK;
            *proto = IPPROTO_TCP;
         }
      
         /* create rpc service handle: tcp only */
         *transp = svctcp_create (sock, 0, 0);
         if (*transp == NULL) {
            gdsError (GDS_ERR_PROG, "cannot create tcp service");
            return -6;
         }
      }


      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: closedown					*/
/*                                                         		*/
/* Procedure Description: closedown routine for port monitors		*/
/*                                                         		*/
/* Procedure Arguments: signal						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void closedown (int sig)
   {
      if (((_shutdown != NULL) && (*_shutdown != 0)) ||
         (MUTEX_TRY (_servermux) != 0)) {
         _serverState = _BUSY;
         (void) signal (SIGALRM, (SIG_PF) closedown);
         (void) alarm (_SHUTDOWN_DELAY / 2);
         return;
      }
   
      if (_serverState == _IDLE) {
         exit (0);
      } 
      else {
         _serverState = _IDLE;
      }
   
      MUTEX_RELEASE (_servermux);
      (void) signal (SIGALRM, (SIG_PF) closedown);
      (void) alarm (_SHUTDOWN_DELAY / 2);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcSetServerBusy				*/
/*                                                         		*/
/* Procedure Description: set rpc server into busy state		*/
/*                                                         		*/
/* Procedure Arguments: busyflag					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   void rpcSetServerBusy (int busyflag)
   {
      MUTEX_GET (_servermux);
      _serverState = (busyflag != 0) ? _BUSY : _IDLE;
      MUTEX_RELEASE (_servermux);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcStartServer				*/
/*                                                         		*/
/* Procedure Description: starts an rpc server				*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - port monitor boolean		*/
/*                      shutdown - shutdown if zero	 		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   void rpcStartServer (int rpcpmstart, int* shutdown) 
   {
      /* install close down procedure if started by a port monitor */
      if (rpcpmstart > 0) {
         MUTEX_CREATE (_servermux);
         _shutdown = shutdown;
         (void) signal (SIGALRM, (SIG_PF) closedown);
         (void) alarm (_SHUTDOWN_DELAY / 2);
      }

      /* wait for rpc calls */
      svc_run();
      /* never reached */
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcRegisterService				*/
/*                                                         		*/
/* Procedure Description: registers an rpc service			*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - port monitor boolean		*/
/*                      transp - transport handle			*/
/*                      proto - protocol type				*/
/*                      prognum - rpc program number 			*/
/*                      progver - rpc version number			*/
/*                      service - service function			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcRegisterService (int rpcpmstart, SVCXPRT* transp, int proto,
                     unsigned long prognum, unsigned long progver, 
                     rpc_dispatch_t service)
   {
      /* port map service */
      if (rpcpmstart != 1) {
         /* stand alone */
         (void) pmap_unset (prognum, progver);
      }
      if (!svc_register (transp, prognum, progver, service, proto)) {
         gdsError (GDS_ERR_PROG, "unable to create rpc service");
         return -1;
      }

      return 0;
   }


/* :IMPORTANT: Since rpc handle have a task context in VxWorks the
   task which calls svc_run() should also be the one which registers
   the service handle */ 


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rpcCBService				*/
/*                                                         		*/
/* Procedure Description: rpc callback service				*/
/*                                                         		*/
/* Procedure Arguments: scheduler descriptor				*/
/*                                                         		*/
/* Procedure Returns: void (sets callbacknum if successful)		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   static void rpcCBService (rpcCBService_t* arg)
   {
      u_long		callbacknum;
   
      /* initialize rpc for VxWorks */

      /* register service */
      if (rpcRegisterCallback (&callbacknum, arg->progver, 
         arg->transport, arg->dispatch) != 0) {
         *arg->prognum = (u_long) -1;
         return;
      }
      /* calback is registered */
      *arg->prognum = callbacknum;
   
      /* invoke rpc service; never to return */
      if (runsvc > 0) {
         return;
      }
      runsvc = 1;
      svc_run();
      gdsDebug ("rpc service has returned");
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcStartCallbackService			*/
/*                                                         		*/
/* Procedure Description: starts a rpc callback service			*/
/*                                                         		*/
/* Procedure Arguments: program number(returned), program version,  	*/
/*			service handle (returned), task ID (returned),	*/
/*                      priotity, dispatch function			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcStartCallbackService (u_long* prognum, u_long progver, 
                     SVCXPRT** transport, taskID_t* tid, int priority,
                     rpc_dispatch_t dispatch)
   {
      struct timespec	tick = {0, _INIT_SLEEP};
      rpcCBService_t	arg;		/* parameters for rpc process */
      int		attr;		/* task attribute */
   
      /* fill argument */
      if (prognum == NULL) {
         return -1;
      }
      *prognum = 0;
      if (transport != NULL) {
         *transport = NULL;
      }
      arg.prognum = prognum;
      arg.progver = progver;
      arg.transport = transport;
      arg.dispatch = dispatch;
   
      /* register and start callback service */
      attr = PTHREAD_CREATE_JOINABLE | PTHREAD_SCOPE_SYSTEM;
      if (taskCreate (attr, priority, tid, "trpcCB",
         (taskfunc_t) &rpcCBService, (taskarg_t) &arg) != 0) {
         return -2;
      }
   
     /* wait for task to finish registration */
      while (*prognum == 0) {
         nanosleep (&tick, NULL);
      }
      if (*prognum == (u_long) -1) {
         return -3;
      }
   
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcStopCallbackService			*/
/*                                                         		*/
/* Procedure Description: stops an rpc callback service			*/
/*                                                         		*/
/* Procedure Arguments: program number(returned), program version,  	*/
/*			service handle (returned)			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcStopCallbackService (u_long prognum, u_long progver, 
                     SVCXPRT* transport, taskID_t tid) 
   {
      /* don't make suicide */
      if (tid == 0) {
         return -1;
      }
      /* unregister service and delete task */
      svc_unregister (prognum, progver);
      /* pthread_cancel (tid); */
      /* pthread_join (tid, NULL);*/
      /* svc_destroy (transport); */
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcRegisterCallback				*/
/*                                                         		*/
/* Procedure Description: registers a callback rpc interface		*/
/*                                                         		*/
/* Procedure Arguments: dispatch function, pointer to store program 	*/
/*			number, version number, transport id		*/
/*                                                         		*/
/* Procedure Returns: service handle if successful, NULL if failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcRegisterCallback (u_long* program, u_long version, 
                     SVCXPRT** transport, rpc_dispatch_t dispatch)
   {
      SVCXPRT*		transp;		/* transport handle */
      u_long		prognum;	/* transient rpc program # */
   
   /* TS-RPC calls */
      {
      /* create transport handle */
         transp = svctcp_create (RPC_ANYSOCK, 0, 0);
         if (transp == NULL) {
            gdsDebug ("cannot create tcp service");
            return -1;
         }
      
      	 /* now try to register */
         prognum = 0x40000000;
         while ((prognum < 0x60000000) && (svc_register (transp, prognum,
               version, dispatch, IPPROTO_TCP) == 0)) {
            prognum++;
         }
         if (prognum >= 0x60000000) {
            svc_destroy (transp);
            gdsDebug ("can't register rpc callback service");
            return -2;
         }
      }
   

      /* copy results and return */
      if (program != NULL) {
         *program = prognum;
      }
      if (transport != NULL) {
         *transport = transp;
      }
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcProbe					*/
/*                                                         		*/
/* Procedure Description: probes an rpc interface			*/
/*                                                         		*/
/* Procedure Arguments: hostname, prog. num, vers. num, netid, timeout 	*/
/*                      client (return)                   		*/
/*                     			                   		*/
/* Procedure Returns: 1 if successful, 0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int rpcProbe (const char* host, const u_long prognum, 
                const u_long versnum, const char* nettype, 
                const struct timeval* timeout, CLIENT** client)
   {
      CLIENT*		clnt;
   
      clnt = clnt_create ((char*) host, prognum, versnum, (char*) nettype);
      if (client != NULL) {
         *client = clnt;
      } 
      else if (clnt != NULL) {
         clnt_destroy (clnt);
      }
      return (clnt != NULL);
   }

