///	\file gsc18ai64.h
///	\brief GSC 16bit, 64 channel 750KS/sec ADC Module Definitions.

///< Subsystem ID to identify and locate module on PCI bus
#define ADC_18AI64_SS_ID  0x3570 

// Function prototypes
int  gsc18ai64Init( CDS_HARDWARE*, struct pci_dev* );
int  gsc18ai64Clock( CDS_HARDWARE*, int );
void gsc18ai64ReadRegisters( CDS_HARDWARE*, int );
void gsc18ai64AdcStop( int );

// Board Control Register (BCR) bits
#define GSA7_RESET 0x8000
#define GSA7_FULL_DIFFERENTIAL 0x200
#define GSA7_AUTO_CAL 0x2000
#define GSA7_AUTO_CAL_PASS 0x4000

// Scan and Sync Control (SSC) register bits
#define GSA7_64_CHANNEL 0x6
#define GSA7_32_CHANNEL 0x5
#define GSA7_16_CHANNEL 0x4
#define GSA7_8_CHANNEL 0x3
#define GSA7_2_CHANNEL 0x1
#define GSA7_ENABLE_CLK_PIN_38 0x200
#define GSA7_SAMPLE_START 0x10000
#define GSA7_EXTERNAL_SYNC 0x10
#define GSA7_ENABLE_X_SYNC 0x80
#define GSA7_CLOCK_ENABLE 0x800

// Production Config Register bits
#define GSA7_FIRMWARE_REV 0xfff
#define GSA7_IS_32_CHANNEL_CARD ( 1 << 12 )
#define GSA7_MASTER_CLK_FREQ ( 3 << 13 )
#define GSA7_INPUT_BW ( 3 << 15 )
#define GSA7_PIN33_IS_CLK ( 1 << 17 )
#define GSA7_LOW_RANGE ( 1 << 18 )
#define GSC18AI64_OSC_FREQ 60000000

// ADC Read info
#define GSA7_DATA_CODE_OFFSET 0x20000
#define GSA7_DATA_MASK 0x3ffff
#define GSA7_FIRST_SAMPLE_MARK 0x40000
