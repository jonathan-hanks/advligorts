/* GSC 20-bit DAC Module Defs ********************************************************* */

int gsc20ao8Init( CDS_HARDWARE* , struct pci_dev* );

#define DAC_20BIT_SS_ID         0x3574  /* Subsystem ID to find module on PCI bus       */
#define GSAO_20BIT_RESET        (1 << 31)
#define GSAO_20BIT_OFFSET_BINARY        (1 << 25)
#define GSAO_20BIT_10VOLT_RANGE (2 << 8)
#define GSAO_20BIT_EXT_CLOCK_SRC        (2 << 12)
#define GSAO_20BIT_EXT_TRIG_SRC (2 << 14)
#define GSAO_20BIT_DIFF_OUTS    (1 << 16)
#define GSAO_20BIT_ENABLE_CLOCK (1 << 5)
#define GSAO_20BIT_SIMULT_OUT   (1 << 18)
#define GSAO_20BIT_DIO_RW       0x80    // Set first nibble write, second read for Watchdog
#define GSAO_20BIT_PRELOAD      64      // Number of data points to preload DAC FIFO on startup (8 chan x 8 values)
#define GSAO_20BIT_MASK         0x1fffff
#define GSAO_20BIT_CHAN_COUNT   8
#define GSAO_20BIT_DMA_LOCAL_ADDR   0x48
#define GSAO_20BIT_AUTOCAL_SET		(1 << 28)
#define GSAO_20BIT_AUTOCAL_PASS		(1 << 29)
#define GSAO_20BIT_OUTPUT_ENABLE       0x80    // Enable DAC Outputs
