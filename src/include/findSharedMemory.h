#ifndef LIGO_FINDSHAREDMEMORY_H 
#define LIGO_FINDSHAREDMEMORY_H

volatile void* findSharedMemory( char* );
volatile void* findSharedMemorySize( char*, int size_mb );

#endif
