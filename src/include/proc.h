#ifndef LIGO_PROC_H
#define LIGO_PROV_H

/* Who owns /proc entries and file permissions */
#define PROC_UID 1001
#define PROC_MODE 0644

#endif
