package gps

/*
#include <stdint.h>
#include <sys/ioctl.h>

#define IOCTL_GPSTIME_TIME 1
#define IOCTL_GPSTIME_PAUSE _IO('x', 2)
#define IOCTL_GPSTIME_RESUME _IO('x', 3)
#define IOCTL_GPSTIME_RESET _IO('x', 4)
#define IOCTL_GPSTIME_STEP _IO('x', 5)

static void gps_now(int fd, int64_t *dest_sec, int32_t *dest_nano) {
	unsigned long t[3];
	ioctl( fd, IOCTL_GPSTIME_TIME, &t);
	t[1] *= 1000;
	t[1] += t[2];
	*dest_sec = t[0];
	*dest_nano = t[1];
}

static void gps_action(int fd, int action) {
	ioctl( fd, action );
}

static void gps_pause(int fd) {
	gps_action(fd, IOCTL_GPSTIME_PAUSE);
}

static void gps_resume(int fd) {
	gps_action(fd, IOCTL_GPSTIME_RESUME);
}

static void gps_step(int fd) {
	gps_action(fd, IOCTL_GPSTIME_STEP);
}

*/
import "C"
import (
	"fmt"
	"os"
	"time"
)

const ioctlStatus = 0
const ioctlTime = 1

type Clock struct {
	gpsFile *os.File
	gpsFd   uintptr
	offset  int64
}

func NewClock() (*Clock, error) {
	gpsFile, err := os.Open("/dev/gpstime")
	if err != nil {
		return nil, err
	}
	return &Clock{
		gpsFile: gpsFile,
		gpsFd:   gpsFile.Fd(),
		offset:  0,
	}, nil
}

func (c *Clock) Close() error {
	if c != nil {
		c.gpsFile.Close()
	}
	return nil
}

func (c *Clock) Now() time.Time {
	var gpsSec C.int64_t
	var gpsNano C.int32_t

	C.gps_now(C.int(c.gpsFd), &gpsSec, &gpsNano)
	return time.Unix(int64(gpsSec+C.int64_t(c.offset)), int64(gpsNano))
}

func (c *Clock) String() string {
	t := c.Now()
	return fmt.Sprintf("%d:%d", t.Unix(), t.Nanosecond())
}

func (c *Clock) Paused() bool {
	f, err := os.Open("/sys/kernel/gpstime/debug_pause_status")
	if err != nil {
		return false
	}
	defer f.Close()
	var buffer [1]byte
	n, err := f.Read(buffer[:])
	if n == 0 || err != nil {
		return false
	}
	return buffer[0] == '1'
}

func (c *Clock) Pause() {
	C.gps_pause(C.int(c.gpsFd))
}

func (c *Clock) Resume() {
	C.gps_resume(C.int(c.gpsFd))
}

func (c *Clock) Step() {
	C.gps_step(C.int(c.gpsFd))
}
