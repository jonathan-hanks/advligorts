import os
import logging


ENV_FILE = os.getenv('RTS_ENV', '/etc/advligorts/env')

USER_VARS = ['SITE', 'IFO', 'RCG_LIB_PATH']
LIST_VARS = ['RTS_VERSION'] + USER_VARS + ['RCG_SRC', 'RCG_BUILD_ROOT', 'RCG_BUILDD', 'RCG_TARGET']
EXPORT_VARS = USER_VARS + ['site', 'ifo', 'CDS_SRC', 'CDS_IFO_SRC']



def check_env():
    for var in USER_VARS:
	if not os.getenv(var):
	    raise ValueError(f"variable '{var}' not set.")
    for var in LIST_VARS:
	logging.info(f"{var}=${!var}")
    for var in EXPORT_VARS:
	#export $var
        pass


def prep_buildd():
    if os.path.exists(RCG_BUILDD):
        return
    logging.info(f"creating RCG_BUILDD {RCG_BUILDD}...")
    if ! mkdir -p "$RCG_BUILDD" 2>/dev/null ; then
        log "Could not create build directory '$RCG_BUILDD'."
        log "Please create manually (with correct permissions) and try again."
        exit 1
    fi
    log "configuring RCG_BUILDD $RCG_BUILDD..."
    cd "$RCG_BUILDD"
    "$RCG_SRC"/configure


def prep_target():
    if os.path.exists(RCG_TARGET):
        return
    logging.info(f"creating RCG_TARGET {RCG_TARGET}...")
    if ! mkdir -p "$RCG_TARGET" 2>/dev/null ; then
        log "Could not create target directory '$RCG_TARGET'."
        log "Please create manually (with correct permissions) and try again."
        exit 1
    fi
    mkdir -p ${RCG_TARGET}/{target,chans/tmp}





def main():



    if error:
	logging.error(f"The following environment variables must be set (in e.g. {ENV_FILE}:")
	for var in USER_VARS:
	    logging.error(f"  {var}")
	    raise ValueError
