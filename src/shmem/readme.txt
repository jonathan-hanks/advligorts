The shmem directory contains header files describing the name, size and structure used for a shared memory block.

The definitive version of the structure is also defined here under a separate file.

shared memories can either be host-wide, or model-specific.
For host-wide shared memories, the full name is given as shmem_<memname>_name.
For model_specific shared memories, a suffix is given as shmem_<memname>_suffix.  Append this suffix to the model name to get the shared memory name.
