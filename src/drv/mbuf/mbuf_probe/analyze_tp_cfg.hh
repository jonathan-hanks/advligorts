//
// Created by erik.vonreis on 5/20/21.
//

#ifndef DAQD_TRUNK_ANALYZE_TP_CFG_HH
#define DAQD_TRUNK_ANALYZE_TP_CFG_HH

#include <cstddef>
#include "mbuf_probe.hh"

namespace analyze
{
    void analyze_tp_cfg( volatile void*    buffer,
        std::size_t       size,
        const ConfigOpts& options );
}

#endif // DAQD_TRUNK_ANALYZE_TP_CFG_HH
