#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <unistd.h>

#include "drv/shmem.h"
#include "mbuf.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SHMEM_TYPE_MBUF 0
#define SHMEM_TYPE_POSIX 1
#define SHMEM_TYPE_WRAPPED -1

typedef struct shmem_mbuf_intl
{
    int fd_;
} shmem_mbuf_intl;

typedef struct shmem_posix_intl
{
    int  fd_;
    char name_[ MBUF_NAME_LEN + 2 ];
} shmem_posix_intl;

typedef struct shmem_wrapped_intl
{
    void ( *close )( volatile void* );
} shmem_wrapped_intl;

typedef struct shmem_intl
{
    int            shmem_type;
    volatile void* mapping_;
    size_t         req_size_;

    void ( *close )( struct shmem_intl* );

    union
    {
        shmem_mbuf_intl    mbuf;
        shmem_posix_intl   posix;
        shmem_wrapped_intl wrapped;
    } intl;
} shmem_intl;

int
shmem_format_name( char* dest, const char* src, size_t n )
{
    char* cur = NULL;

    static const char* prefix = "/rtl_mem_";
    size_t             dest_len = 0;
    int                tmp = 0;

    if ( !dest || !src || n < 1 )
        return 0;
    if ( snprintf( dest, n, "%s%s", prefix, src ) > n )
        return 0;
    for ( cur = dest; *cur; ++cur )
    {
        *cur = (char)tolower( *cur );
    }
    return 1;
}

extern const char*
shmem_name_parse( const char* input, const char** prefix )
{
    const char* dummy = NULL;
    if ( !input )
    {
        return NULL;
    }
    if ( !prefix )
    {
        prefix = &dummy;
    }
    if ( strncmp( input, SHMEM_MBUF_PREFIX, strlen( SHMEM_MBUF_PREFIX ) ) == 0 )
    {
        *prefix = SHMEM_MBUF_PREFIX;
        return input + strlen( SHMEM_MBUF_PREFIX );
    }
    else if ( strncmp( input,
                       SHMEM_POSIX_PREFIX,
                       strlen( SHMEM_POSIX_PREFIX ) ) == 0 )
    {
        *prefix = SHMEM_POSIX_PREFIX;
        return input + strlen( SHMEM_POSIX_PREFIX );
    }
    *prefix = SHMEM_MBUF_PREFIX;
    return input;
}

static void
shmem_close_mbuf( shmem_intl* intl )
{
    if ( intl )
    {
        if ( intl->mapping_ != MAP_FAILED )
        {
            munmap( (void*)( intl->mapping_ ), intl->req_size_ );
        }
        if ( intl->intl.mbuf.fd_ )
        {
            close( intl->intl.mbuf.fd_ );
        }
    }
}

static shmem_handle
shmem_open_mbuf( const char* sys_name, size_t size_mb )
{
    shmem_intl* intl = NULL;
    int         buffer_len = 0;
    size_t      name_len = 0;
    size_t      req_size = size_mb * 1024 * 1024;

    name_len = strlen( sys_name );
    if ( name_len == 0 || name_len > MBUF_NAME_LEN )
    {
        fprintf( stderr, "mbuf name NULL or too long\n" );
        return NULL;
    }

    intl = malloc( sizeof( shmem_intl ) );
    if ( !intl )
    {
        fprintf( stderr, "Failed to allocate for internal structure for tracking mbuf.\n" );
        return NULL;
    }
    memset( intl, 0, sizeof( *intl ) );
    intl->shmem_type = SHMEM_TYPE_MBUF;
    intl->mapping_ = MAP_FAILED;
    intl->close = shmem_close_mbuf;

    if ( ( intl->intl.mbuf.fd_ = open( "/dev/mbuf", O_RDWR | O_SYNC ) ) < 0 )
    {
        fprintf( stderr, "Couldn't open /dev/mbuf read/write\n" );
        goto cleanup;
    }
    struct mbuf_request_struct req;
    req.size = req_size;
    strcpy( req.name, sys_name );
    buffer_len = ioctl( intl->intl.mbuf.fd_, IOCTL_MBUF_ALLOCATE, &req );

    if ( buffer_len < (int)req_size )
    {
        fprintf( stderr, "Buffer length was less than the requested size\n" );
        goto cleanup;
    }
    intl->req_size_ = req_size;
    intl->mapping_ = (volatile void*)mmap( 0,
                                           req_size,
                                           PROT_READ | PROT_WRITE,
                                           MAP_SHARED,
                                           intl->intl.mbuf.fd_,
                                           0 );
    if ( intl->mapping_ == MAP_FAILED )
    {
        fprintf( stderr, "Mapping to mbuf failed\n" );
        goto cleanup;
    }
    /* printf(" %s mmapped address is 0x%lx\n", sys,(long)addr); */
    return (void*)intl;

cleanup:
    shmem_close( intl );
    return NULL;
}

static void
shmem_close_posix( shmem_intl* intl )
{
    if ( intl )
    {
        if ( intl->mapping_ != MAP_FAILED )
        {
            munmap( (void*)intl->mapping_, intl->req_size_ );
        }
        if ( intl->intl.posix.fd_ >= 0 )
        {
            close( intl->intl.posix.fd_ );
        }
        if ( strcmp( intl->intl.posix.name_, "" ) != 0 )
        {
            // shm_unlink( intl->intl.posix.name_ );
        }
    }
}

static shmem_handle
shmem_open_posix( const char* sys_name, size_t size_mb )
{
    shmem_intl* intl = NULL;
    size_t      name_len = 0;
    size_t      req_size = size_mb * 1024 * 1024;
    int         status = 0;
    struct stat statbuf;

    name_len = strlen( sys_name );
    if ( name_len == 0 || name_len > MBUF_NAME_LEN )
    {
        return NULL;
    }

    intl = malloc( sizeof( shmem_intl ) );
    if ( !intl )
    {
        return NULL;
    }
    memset( intl, 0, sizeof( *intl ) );
    intl->shmem_type = SHMEM_TYPE_POSIX;
    intl->mapping_ = MAP_FAILED;
    intl->close = shmem_close_posix;

    snprintf( intl->intl.posix.name_, MBUF_NAME_LEN + 2, "/%s", sys_name );

    intl->intl.posix.fd_ =
        shm_open( intl->intl.posix.name_, O_RDWR | O_CREAT, 0666 );
    if ( intl->intl.posix.fd_ < 0 )
    {
        goto cleanup;
    }
    status = fstat( intl->intl.posix.fd_, &statbuf );
    if ( status )
    {
        goto cleanup;
    }
    if ( statbuf.st_size == 0 )
    {
        status = ftruncate( intl->intl.posix.fd_, req_size );
        if ( status )
        {
            goto cleanup;
        }
    }
    else if ( statbuf.st_size < req_size )
    {
        goto cleanup;
    }
    intl->req_size_ = req_size;

    intl->mapping_ = (volatile void*)mmap( 0,
                                           req_size,
                                           PROT_READ | PROT_WRITE,
                                           MAP_SHARED,
                                           intl->intl.mbuf.fd_,
                                           0 );
    if ( intl->mapping_ == MAP_FAILED )
    {
        goto cleanup;
    }

    return intl;
cleanup:
    shmem_close( intl );
    return NULL;
}

static void
shmem_close_wrapped( shmem_intl* intl )
{
    if ( intl && intl->intl.wrapped.close )
    {
        intl->intl.wrapped.close( intl->mapping_ );
    }
}

shmem_handle
shmem_open( const char* sys_name, size_t size_mb )
{
    if ( !sys_name || size_mb == 0 )
        return NULL;

    if ( strncmp( sys_name, SHMEM_MBUF_PREFIX, strlen( SHMEM_MBUF_PREFIX ) ) ==
         0 )
    {
        return shmem_open_mbuf( sys_name + strlen( SHMEM_MBUF_PREFIX ),
                                size_mb );
    }
    else if ( strncmp( sys_name,
                       SHMEM_POSIX_PREFIX,
                       strlen( SHMEM_POSIX_PREFIX ) ) == 0 )
    {
        return shmem_open_posix( sys_name + strlen( SHMEM_POSIX_PREFIX ),
                                 size_mb );
    }
    return shmem_open_mbuf( sys_name, size_mb );
}

void
shmem_close( shmem_handle handle )
{
    shmem_intl* intl = (shmem_intl*)handle;
    if ( intl )
    {
        intl->close( intl );
        free( intl );
    }
}

volatile void*
shmem_mapping( shmem_handle handle )
{
    shmem_intl* intl = (shmem_intl*)handle;
    return ( intl ? intl->mapping_ : NULL );
}

size_t
shmem_size_mb( shmem_handle handle )
{
    shmem_intl* intl = (shmem_intl*)handle;
    return ( intl ? intl->req_size_/(1024*1024) : 0 );
}

shmem_handle
shmem_wrap_memory_arbitrary( volatile void* data,
                             size_t         size,
                             void ( *cleanup )( volatile void* ) )
{
    shmem_intl* intl = malloc( sizeof( shmem_intl ) );
    if ( !intl )
    {
        return NULL;
    }
    memset( intl, 0, sizeof( *intl ) );
    intl->mapping_ = data;
    intl->req_size_ = size;
    intl->close = shmem_close_wrapped;
    intl->intl.wrapped.close = cleanup;
    return intl;
}

#ifdef __cplusplus
}
#endif