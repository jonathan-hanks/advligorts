#include <linux/module.h>
#include <linux/init.h>

#include "gpstime_kernel.h"

//  Define the module metadata.
#define MODULE_NAME "gpstime_stress_and_verify"
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("A module to stress test the ligo_gpstime_get_ts() function in the gpstime module");
MODULE_VERSION("1.0");

int MP_NUM_REQS = 1e6;
module_param(MP_NUM_REQS, int, S_IRUSR);

static int __init greeter_init(void)
{
    int i = 0;
    pr_info("%s: module loaded at 0x%p\n", MODULE_NAME, greeter_init);

    LIGO_TIMESPEC  new_ts, last_ts;

/*
    for(i = 0; i < MP_NUM_REQS; ++i)
    {
        ligo_gpstime_get_ts(&last_ts);
        pr_info("%s: gpstime is %lld, %lu\n", MODULE_NAME, last_ts.tv_sec, last_ts.tv_nsec);
    }*/


    
    ligo_gpstime_get_ts(&last_ts);

    for(i = 0; i < MP_NUM_REQS; ++i)
    {
        ligo_gpstime_get_ts(&new_ts);
        if( timespec64_compare(&new_ts, &last_ts) < 0 )
        {
            pr_info("%s: Error time queried after a later time, run index %d, last_ts : {%lld, %lu}, new_ts: {%lld, %lu}\n", 
                    MODULE_NAME, 
                    i,
                    last_ts.tv_sec,
                    last_ts.tv_nsec,
                    new_ts.tv_sec,
                    new_ts.tv_nsec);
        }
        ligo_gpstime_get_ts(&last_ts);
    }

    return 0;
}

static void __exit greeter_exit(void)
{
    pr_info("%s: Test Complete\n", MODULE_NAME);
    pr_info("%s: module unloaded from 0x%p\n", MODULE_NAME, greeter_exit);
}

module_init(greeter_init);
module_exit(greeter_exit);
