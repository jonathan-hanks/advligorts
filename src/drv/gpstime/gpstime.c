//#include <linux/config.h>
#include <linux/delay.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>
#include <linux/time.h>
#include <linux/device.h>

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>

#ifdef MODVERSIONS
#  include <linux/modversions.h>
#endif
#include <asm/io.h>
#include "gpstime.h"
#include "gpstime_kernel.h"
#include "timespec_utils.h"

#include "../../include/drv/cdsHardware.h"
#include "../../include/proc.h"

CDS_HARDWARE cdsPciModules;

#define NANO_IN_SEC 1000000000

#define IN_LIGO_GPS_KERNEL_DRIVER 1

#include "../../include/drv/spectracomGPS.c"
#include "../../include/drv/symmetricomGps.c"
#include "../../include/drv/ligoPcieTiming_core.c"

#define CDS_LOCAL_GPS_BUFFER_SIZE 30



/* /proc/gps entry */
struct proc_dir_entry *proc_gps_entry;
/* /proc/gps_offset entry */
struct proc_dir_entry *proc_gps_offset_entry;

atomic64_t gps_offset = ATOMIC_INIT(0);

typedef struct gps_pause_info {
    ligo_duration pause_offset;
    LIGO_TIMESPEC pause_time;
    ligo_duration pause_step;
    int paused;
} gps_pause_info;

#define PAUSE_STATE_REALTIME 0
#define PAUSE_STATE_PAUSED 1
#define PAUSE_STATE_BEHIND 2

// track if we have had a pause operation ever
// this is reset on a reset.
static atomic64_t gps_has_been_paused = ATOMIC_INIT(0);
static DEFINE_SPINLOCK(gps_pause_lock);
static gps_pause_info pause_info;

static inline void
gps_pause_init(void)
{
    spin_lock(&gps_pause_lock);
    atomic64_set(&gps_has_been_paused, 0);
    pause_info.pause_offset.tv_sec = 0;
    pause_info.pause_offset.tv_nsec = 0;
    pause_info.pause_time.tv_sec = 0;
    pause_info.pause_time.tv_nsec = 0;
    pause_info.pause_step.tv_sec = 0;
    pause_info.pause_step.tv_nsec = (NANO_IN_SEC/16);
    pause_info.paused = 0;
    spin_unlock(&gps_pause_lock);
}

static inline int
gps_pause_state(void)
{
    int state = PAUSE_STATE_REALTIME;
    spin_lock(&gps_pause_lock);
    if (pause_info.paused)
    {
        state = PAUSE_STATE_PAUSED;
    }
    else if (pause_info.pause_offset.tv_sec != 0 || pause_info.pause_offset.tv_nsec != 0)
    {
        state = PAUSE_STATE_BEHIND;
    }
    spin_unlock(&gps_pause_lock);
    return state;
}

static inline void
gps_pause_pause(void)
{
    LIGO_TIMESPEC real_time;
    ligo_get_current_kernel_time(&real_time);
    spin_lock(&gps_pause_lock);
    atomic64_set(&gps_has_been_paused, 1);
    if (pause_info.paused == 0)
    {
        pause_info.paused = 1;
        pause_info.pause_time = timespec_sub(real_time, pause_info.pause_offset);
    }
    spin_unlock(&gps_pause_lock);
}

static inline void
gps_pause_resume(void)
{
    LIGO_TIMESPEC real_time;
    ligo_get_current_kernel_time(&real_time);
    spin_lock(&gps_pause_lock);
    if (pause_info.paused)
    {
        pause_info.paused = 0;
        pause_info.pause_offset = timespec_difference(real_time, pause_info.pause_time);
    }
    spin_unlock(&gps_pause_lock);
}

static inline void
gps_pause_step(void)
{
    spin_lock(&gps_pause_lock);
    if (pause_info.paused)
    {
        pause_info.pause_time = timespec_add(pause_info.pause_time, pause_info.pause_step);
    }
    spin_unlock(&gps_pause_lock);
}

static inline ligo_duration
gps_pause_get_step_size(void)
{
    ligo_duration result;
    spin_lock(&gps_pause_lock);
    result = pause_info.pause_step;
    spin_unlock(&gps_pause_lock);
    return result;
}

static inline void
gps_pause_set_step_size(ligo_duration step_size)
{
    if (step_size.tv_sec < 0 || step_size.tv_nsec < 0)
    {
        return;
    }
    if (step_size.tv_nsec >= NANO_IN_SEC)
    {
        step_size.tv_sec += step_size.tv_nsec / NANO_IN_SEC;
        step_size.tv_nsec = step_size.tv_nsec % NANO_IN_SEC;
    }
    spin_lock(&gps_pause_lock);
    pause_info.pause_step = step_size;
    spin_unlock(&gps_pause_lock);
}

static inline LIGO_TIMESPEC
gps_pause_now(void)
{
    LIGO_TIMESPEC result;
    LIGO_TIMESPEC real_time;

    // stay lock free if we have never been paused
    if (atomic64_read(&gps_has_been_paused) == 0)
    {
        ligo_get_current_kernel_time(&result);
        return result;
    }

    spin_lock( &gps_pause_lock );
    if ( pause_info.paused )
    {
        result = pause_info.pause_time;
    }
    else
    {
        ligo_get_current_kernel_time( &real_time );
        result = timespec_sub( real_time, pause_info.pause_offset );
    }
    spin_unlock( &gps_pause_lock );
    return result;
}

/* What type of syncing does the driver need */
static int gps_module_sync_type = STATUS_SYMMETRICOM_NO_SYNC;

/* character device structures */
static dev_t symmetricom_dev = MKDEV( 0, 0 );
static struct class* gpstime_class = NULL;
static struct device* gpstime_device = NULL;
static struct cdev symmetricom_cdev;
static int card_present;
static int card_type; /* 0 - symmetricom; 1 - spectracom; 2 - LIGO PCI */

/* methods of the character device */
static int symmetricom_open(struct inode *inode, struct file *filp);
static int symmetricom_release(struct inode *inode, struct file *filp);
static long symmetricom_ioctl(struct file *inode, unsigned int cmd, unsigned long arg);

/* the file operations, i.e. all character device methods */
static struct file_operations symmetricom_fops = {
        .open = symmetricom_open,
        .release = symmetricom_release,
        .unlocked_ioctl = symmetricom_ioctl,
        .owner = THIS_MODULE,
};

/* character device open method */
static int symmetricom_open(struct inode *inode, struct file *filp)
{
        return 0;
}


/* character device last close method */
static int symmetricom_release(struct inode *inode, struct file *filp)
{
        return 0;
}


// Read current GPS time from the card
int get_cur_time(unsigned long *req) {
  long offset = 0;
  unsigned int timeSec = 0;
  unsigned int timeUsec = 0;
  int sync = 0;

  offset = atomic64_read(&gps_offset);

  if (card_present && card_type == GPSTIME_SPECTRACOM_CARD) {
	sync = getGpsTimeTsync(&timeSec, &timeUsec);
	req[0] = timeSec; req[1] = timeUsec; req[2] = 0;
  } else if (card_present && card_type == GPSTIME_SYMMETRICOM_CARD) {
  	lockGpsTime();
	sync = getGpsTime(&timeSec, &timeUsec);
	req[0] = timeSec; req[1] = timeUsec; req[2] = 0;
  } else if (card_present && card_type == GPSTIME_LIGO_PCI_CARD) { 
        sync = lptc_get_gps_time(&cdsPciModules, &timeSec, &timeUsec);
        req[0] = timeSec; req[1] = timeUsec; req[2] = 0;
  } else {
      // Get current kernel time (in GPS)
      LIGO_TIMESPEC t;
      t = gps_pause_now();
      req[0] = t.tv_sec; req[1] = t.tv_nsec/1000; req[2] = t.tv_nsec%1000;
      sync = 1;
  }
  req[0] += offset;
  return sync;
}

static long symmetricom_ioctl(struct file *inode, unsigned int cmd, unsigned long arg)
{
    unsigned long req[3];
    unsigned long res = 0;

    /* printk("symmetricom_ioctl called cmd = %d arg = %d", (int)cmd, (int)arg); */
    switch(cmd){
        case IOCTL_SYMMETRICOM_STATUS:
            res = get_cur_time(req);
            if (copy_to_user ((void *) arg, &res,  sizeof (res))) return -EFAULT;
            break;
        case IOCTL_SYMMETRICOM_TIME:
            get_cur_time(req);
            if (copy_to_user ((void *) arg, req,  sizeof (req))) return -EFAULT;
		    break;
        case IOCTL_GPSTIME_PAUSE:
            if (card_present)
            {
                return -EINVAL;
            }
            gps_pause_pause();
            break;
        case IOCTL_GPSTIME_RESUME:
            if (card_present)
            {
                return -EINVAL;
            }
            gps_pause_resume();
            break;
        case IOCTL_GPSTIME_RESET:
            if (card_present)
            {
                return -EINVAL;
            }
            gps_pause_init();
            break;
        case IOCTL_GPSTIME_STEP:
            if (card_present)
            {
                return -EINVAL;
            }
            gps_pause_step();
            break;
        default:
                return -EINVAL;
        }
        return -EINVAL;
}

/* This is called to translate an item in the sequence to output.
 * In the /proc/gps, there is only one item, and it is the current
 * gps time.
 *
 * So this is called to read the GPS time
 */
static int gps_seq_show(struct seq_file *s, void *v)
{
	unsigned long req[3];
	get_cur_time(req);
	seq_printf(s, "%ld.%02ld\n", req[0], req[1]/10000);
	return 0;
}

static int gps_open(struct inode *inode, struct file *file)
{
	return single_open(file, gps_seq_show, NULL);
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 6, 0)
static struct file_operations gps_file_ops = {
        .owner = THIS_MODULE,
        .open = gps_open,
        .read = seq_read,
        .llseek = seq_lseek,
        .release = single_release
};
#else
static struct proc_ops gps_file_ops = {
	.proc_open = gps_open,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release
};
#endif

/* The output buffer provided to all sysfs calls here is PAGE_SIZED (ie typically 4k bytes) */

// create sysfs_emit functions if they do not exist (kernel too old)
#ifndef syfs_emit
/**
 *	sysfs_emit - scnprintf equivalent, aware of PAGE_SIZE buffer.
 *	@buf:	start of PAGE_SIZE buffer.
 *	@fmt:	format
 *	@...:	optional arguments to @format
 *
 *
 * Returns number of characters written to @buf.
 */
int sysfs_emit(char *buf, const char *fmt, ...)
{
    va_list args;
    int len;

    if (WARN(!buf || offset_in_page(buf),
               "invalid sysfs_emit: buf:%p\n", buf))
        return 0;

    va_start(args, fmt);
    len = vscnprintf(buf, PAGE_SIZE, fmt, args);
    va_end(args);

    return len;
}
#endif //sysfs_emit

#ifndef sysfs_emit_at
/**
 *	sysfs_emit_at - scnprintf equivalent, aware of PAGE_SIZE buffer.
 *	@buf:	start of PAGE_SIZE buffer.
 *	@at:	offset in @buf to start write in bytes
 *		@at must be >= 0 && < PAGE_SIZE
 *	@fmt:	format
 *	@...:	optional arguments to @fmt
 *
 *
 * Returns number of characters written starting at &@buf[@at].
 */
int sysfs_emit_at(char *buf, int at, const char *fmt, ...)
{
	va_list args;
	int len;

	if (WARN(!buf || offset_in_page(buf) || at < 0 || at >= PAGE_SIZE,
		 "invalid sysfs_emit_at: buf:%p at:%d\n", buf, at))
		return 0;

	va_start(args, fmt);
	len = vscnprintf(buf + at, PAGE_SIZE - at, fmt, args);
	va_end(args);

	return len;
}
#endif //sysfs_emit_at

static ssize_t gpstime_sysfs_card_present_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "%d\n", (int)card_present);
}

static ssize_t gpstime_sysfs_card_type_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "%d\n", (int)card_type);
}

static ssize_t gpstime_sysfs_gps_offset_type_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "%d\n", (int)gps_module_sync_type);
}

static ssize_t gpstime_sysfs_gps_offset_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "%lld\n", (long long)atomic64_read(&gps_offset));
}

static ssize_t gpstime_sysfs_gps_offset_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	long long new_offset = 0;
	char localbuf[CDS_LOCAL_GPS_BUFFER_SIZE+1];
    size_t full_count = count;
    int conv_ret = 0;

    memset(localbuf, 0, CDS_LOCAL_GPS_BUFFER_SIZE+1);
    // The symmetricom driver doesn't need a tweak on the timing, so don't allow non-zero tweaks
    if (card_present && card_type == GPSTIME_SYMMETRICOM_CARD) {
        printk("gpstime_sysfs_gps_offset_store called when a symmetricom card is present, ignoring the value");
        return full_count;
    }
    // just ignore input when it is too big
	if (count >= CDS_LOCAL_GPS_BUFFER_SIZE) {
        printk("gpstime_sysfs_gps_offset_store called with too much input, ignoring the value");
        return -EFAULT;
    }
    memcpy(localbuf, buf, count);
	localbuf[count] = '\0';

    if ((conv_ret = kstrtoll(localbuf, 10, &new_offset)) != 0) {
        printk("gpstime_sysfs_gps_offset_store error converting offset into an integer - %s %d", (conv_ret == -ERANGE ? "range" : "overflow"), conv_ret);
        return -EFAULT;
    }
    atomic64_set(&gps_offset, new_offset);
    printk("gpstime_sysfs_gps_offset_store success - new value = %lld", new_offset);
    /* tell it we consumed everything */
	return (ssize_t)count;
}

static ssize_t gpstime_sysfs_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    unsigned long req[3];
    int status;
    if(card_type == GPSTIME_LIGO_PCI_CARD) 
    {
        status = lptc_get_lptc_status(&cdsPciModules);
    }
    else
    {
        status = (int)get_cur_time( req );
    }
    return sysfs_emit(buf, "%d\n", status);
}

static ssize_t gpstime_sysfs_gpstime_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
    unsigned long req[3];
    get_cur_time(req);
    return sysfs_emit(buf, "%ld.%02ld\n", req[0], req[1]/10000);
}

static ssize_t gpstime_sysfs_pause_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
    switch (gps_pause_state())
    {
    case PAUSE_STATE_REALTIME: return sysfs_emit(buf, "%d realtime\n", PAUSE_STATE_REALTIME);
    case PAUSE_STATE_PAUSED: return sysfs_emit(buf, "%d paused\n", PAUSE_STATE_PAUSED);
    case PAUSE_STATE_BEHIND: return sysfs_emit(buf, "%d behind\n", PAUSE_STATE_BEHIND);
    }
    return sysfs_emit(buf, "-1 unknown state\n");
}

static ssize_t gpstime_sysfs_pause_step_size_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    ligo_duration step_size = gps_pause_get_step_size();
    s64 step_size_nano = (step_size.tv_sec * NANO_IN_SEC) + step_size.tv_nsec;
    return sysfs_emit(buf, "%lld\n", step_size_nano);
}

static ssize_t gpstime_sysfs_pause_step_size_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
    ligo_duration new_step;
    s64 new_step_nano = 0;
    int conv_ret = 0;

    if ((conv_ret = kstrtoll(buf, 10, &new_step_nano)) != 0) {
        printk("gpstime_sysfs_pause_step_size_store error converting offset into an integer - %s %d", (conv_ret == -ERANGE ? "range" : "overflow"), conv_ret);
        return -EFAULT;
    }
    new_step.tv_sec = new_step_nano / NANO_IN_SEC;
    new_step.tv_nsec = new_step_nano % NANO_IN_SEC;
    gps_pause_set_step_size(new_step);

    /* tell it we consumed everything */
    return (ssize_t)count;
}

static void config_proc_entry(struct proc_dir_entry *proc_entry, int uid)
{
	/* At kernel 3.10+ these helper functions become available
	* and the proc_dir_entry becomes an opaque structure
	*/
#if	LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0)
	proc_set_size(proc_entry , 128);
	proc_set_user(proc_entry, KUIDT_INIT(uid), KGIDT_INIT(0));
#else
	proc_entry->size = 128;
	proc_entry->uid = uid;
	proc_entry->gid = 0;
#endif
}

/**
 * ligo_get_gps_driver_offset
 * @note Exported interface to return the current gps_offset value within the kernel.
 * @return a long containing the current offset that this driver needs to compute the correct gps time.  This function
 * does not give any information regarding the epoch that is used, it is specific to the hardware in the machine.
 * It is exported to help internal LIGO drivers.
 */
long ligo_get_gps_driver_offset(void)
{
    return atomic64_read(&gps_offset);
}

/**
 * @brief This function returns the time of the system by querying GPS
 *        card that is present. If no card is found the system time is
 *        returned.
 *
 *        This function is used by kernel space FE models who would 
 *        like to query the current GPS time.
 *
 * @param ts A pointer to the LIGO_TIMESPEC (timespec/timespec64) struct 
 *           that should be filled with the current time.
 *
 * @return If no card is present the function will return the system time
 *         If an unsupported card_type has been set, the function will return 
 *         (0 + gps_offset) seconds and 0 nanoseconds. This should be seen as 
 *         an error case, and new logic should be added for the new card.
 */
int ligo_gpstime_get_ts(LIGO_TIMESPEC * ts)
{
    long offset = 0;
    unsigned int timeSec = 0;
    unsigned int timeUsec = 0, save = 0;
    int sync = 0;

    offset = atomic64_read(&gps_offset);

    //
    // If we don't have a GPS card we return the system time
    if( !card_present )
    {
        ligo_get_current_kernel_time(ts);
        ts->tv_sec += offset;
        return 1; 
    }


    switch (card_type)
    {
        case GPSTIME_SPECTRACOM_CARD:
            sync = getGpsTimeTsync(&timeSec, &timeUsec);
            save = timeUsec;
            break;
        case GPSTIME_SYMMETRICOM_CARD:
            lockGpsTime();
            sync = getGpsTime(&timeSec, &timeUsec);
            break;
        case GPSTIME_LIGO_PCI_CARD:
            sync = lptc_get_gps_time(&cdsPciModules, &timeSec, &timeUsec);
            break;
        default:
            break;
    }

    ts->tv_sec = timeSec;
    ts->tv_nsec = timeUsec*1000;
    ts->tv_sec += offset;
    return sync;
}

/* sysfs related structures */
static struct kobject *gpstime_sysfs_dir = NULL;

/* Individual sysfs attributes (ie files) */
static struct kobj_attribute sysfs_card_present_attr = __ATTR(card_present, 0444, gpstime_sysfs_card_present_show, NULL);
static struct kobj_attribute sysfs_card_type_attr = __ATTR(card_type, 0444, gpstime_sysfs_card_type_show, NULL);
static struct kobj_attribute sysfs_gps_offset_type_attr = __ATTR(offset_type, 0444, gpstime_sysfs_gps_offset_type_show, NULL);
static struct kobj_attribute sysfs_gps_offset_attr = __ATTR(offset, 0644, gpstime_sysfs_gps_offset_show, gpstime_sysfs_gps_offset_store);
static struct kobj_attribute sysfs_status_attr = __ATTR(status, 0444, gpstime_sysfs_status_show, NULL);
static struct kobj_attribute sysfs_gpstime_attr = __ATTR(time, 0444, gpstime_sysfs_gpstime_show, NULL);
static struct kobj_attribute sysfs_pause_status_attr = __ATTR(debug_pause_status, 0444, gpstime_sysfs_pause_status_show, NULL);
static struct kobj_attribute sysfs_pause_step_size_attr = __ATTR(debug_pause_step_size, 0644, gpstime_sysfs_pause_step_size_show, gpstime_sysfs_pause_step_size_store);

/* group the attributes together for bulk operations */
static struct attribute *gpstime_fields[] = {
        &sysfs_card_present_attr.attr,
        &sysfs_card_type_attr.attr,
        &sysfs_gps_offset_type_attr.attr,
        &sysfs_gps_offset_attr.attr,
        &sysfs_status_attr.attr,
        &sysfs_gpstime_attr.attr,
        &sysfs_pause_status_attr.attr,
        &sysfs_pause_step_size_attr.attr,
        NULL,
};

static struct attribute_group gpstime_attr_group = {
        .attrs = gpstime_fields,
};

#include "lptc_sysfs.c"

/* module initialization - called at module load time */
static int __init symmetricom_init(void)
{
    int ret = 0;
    int card_needs_sync = STATUS_SYMMETRICOM_NO_SYNC;
    struct pci_dev *symdev = NULL;
    proc_gps_entry = NULL;

    /* get the major number of the character device */
    if ((ret = alloc_chrdev_region(&symmetricom_dev, 0, 1, "gpstime")) < 0) {
            printk(KERN_ERR "could not allocate major number for symmetricom\n");
            goto out;
    }

    if (IS_ERR(gpstime_class = class_create( THIS_MODULE, "ligo_gpstime" )))
    {
            printk(KERN_ERR "could not allocate device class for gpstime\n");
            goto out_unalloc_region;
    }

    if (IS_ERR(gpstime_device = device_create( gpstime_class, NULL, symmetricom_dev, NULL, "gpstime" )))
    {
            printk(KERN_ERR "could not create device file for gpstime\n");
            goto out_cleanup_class;
    }

    /* initialize the device structure and register the device with the kernel */
    cdev_init(&symmetricom_cdev, &symmetricom_fops);
    if ((ret = cdev_add(&symmetricom_cdev, symmetricom_dev, 1)) < 0) {
            printk(KERN_ERR "could not allocate chrdev for buf\n");
            goto out_cleanup_device;
    }

    // Create /proc/gps filesystem tree
	proc_gps_entry = proc_create("gps", PROC_MODE | S_IFREG | S_IRUGO, NULL, &gps_file_ops);
        if (proc_gps_entry == NULL) {
                printk(KERN_ALERT "Error: Could not initialize /proc/gps\n");
                goto out_unalloc_region;
        }
	config_proc_entry(proc_gps_entry, PROC_UID);

    gpstime_sysfs_dir = kobject_create_and_add("gpstime", kernel_kobj);
    if (gpstime_sysfs_dir == NULL) {
        printk(KERN_ERR "Could not create /sys/kernel/gpstime directory!\n");
        goto out_remove_proc_entry;
    }
    if (sysfs_create_group(gpstime_sysfs_dir, &gpstime_attr_group) != 0) {
        printk(KERN_ERR "Could not create /sys/kernel/gpstime/... fields!\n");
        goto out_remove_proc_entry;
    }

	/* find the Symmetricom device */
        symdev = pci_get_device (LPTC_VID, LPTC_TID, 0);
        if(symdev)
        {
            printk( "LIGO PCIe Timing Card on bus %x; device %x\n",
                    symdev->bus->number,
                    PCI_SLOT( symdev->devfn ) );
            card_present = 1;
            card_type = GPSTIME_LIGO_PCI_CARD;

            if( add_lptc_sys_files(gpstime_sysfs_dir) )
            {
                goto out_remove_proc_entry;
            }
        }
        else
        {
            symdev = pci_get_device( SYMCOM_VID, SYMCOM_BC635_TID, 0 );
            if ( symdev )
            {
                printk( "Symmetricom GPS card on bus %x; device %x\n",
                        symdev->bus->number,
                        PCI_SLOT( symdev->devfn ) );
                card_present = 1;
                card_type = GPSTIME_SYMMETRICOM_CARD;
            }
            else
            {
                ret = 0;
                // printk("Symmetricom GPS card not found\n");
                // goto out_unalloc_region;
                card_present = 0;
            }
            if ( !card_present )
            {
                /* Try looking for Spectracom GPS card */
                symdev = pci_get_device( TSYNC_VID, TSYNC_TID, 0 );
                if ( symdev )
                {
                    printk( "Spectracom GPS card on bus %x; device %x\n",
                            symdev->bus->number,
                            PCI_SLOT( symdev->devfn ) );
                    card_present = 1;
                    card_type = GPSTIME_SPECTRACOM_CARD;
                }
                else
                {
                    ret = 0;
                    // printk("Symmetricom GPS card not found\n");
                    // goto out_unalloc_region;
                    card_present = 0;
                }
            }
        }

	if (!card_present) {
        printk("Symmetricom/Spectracom GPS card not found\n");
        gps_module_sync_type = STATUS_SYMMETRICOM_LOCALTIME_SYNC;
	} else if (card_type == GPSTIME_SPECTRACOM_CARD) {
            spectracomGpsInitCheckSync(&cdsPciModules, symdev, &card_needs_sync);
            gps_module_sync_type = ( card_needs_sync ? STATUS_SYMMETRICOM_YEAR_SYNC : STATUS_SYMMETRICOM_NO_SYNC);
	} else if (card_type == GPSTIME_SYMMETRICOM_CARD) {
	    symmetricomGpsInit(&cdsPciModules, symdev);
            gps_module_sync_type = STATUS_SYMMETRICOM_NO_SYNC;
	} else if (card_type == GPSTIME_LIGO_PCI_CARD) {
            lptcInit(&cdsPciModules, symdev);
            gps_module_sync_type = STATUS_SYMMETRICOM_NO_SYNC;
        }

        gps_pause_init();

        return ret;
out_remove_proc_entry:
    if (gpstime_sysfs_dir != NULL) {
        kobject_del(gpstime_sysfs_dir);
        gpstime_sysfs_dir = NULL;
    }
    remove_proc_entry("gps", NULL);
out_cleanup_device:
    device_destroy( gpstime_class, symmetricom_dev );
out_cleanup_class:
    class_destroy( gpstime_class );
out_unalloc_region:
        unregister_chrdev_region(symmetricom_dev, 1);
out:
        return ret;
}

/* module unload */
static void __exit symmetricom_exit(void)
{
    if (gpstime_sysfs_dir != NULL) {
        kobject_del(gpstime_sysfs_dir);
        gpstime_sysfs_dir = NULL;
    }
    remove_proc_entry("gps", NULL);
    device_destroy( gpstime_class, symmetricom_dev );
    class_destroy( gpstime_class );
    cdev_del(&symmetricom_cdev);
    unregister_chrdev_region(symmetricom_dev, 1);
}

EXPORT_SYMBOL(ligo_get_gps_driver_offset);
EXPORT_SYMBOL(ligo_gpstime_get_ts);
module_init(symmetricom_init);
module_exit(symmetricom_exit);
MODULE_DESCRIPTION("LIGO GPS time driver.  Contains support for using Symmetricom/Spectricom cards and system time.");
MODULE_AUTHOR("Alex Ivanov aivanov@ligo.caltech.edu");
MODULE_LICENSE("Dual BSD/GPL");

