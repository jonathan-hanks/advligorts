///	@file src/drv/rfm.c
/// 	@brief Routines for finding shared memory locations.

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>

#include "mbuf/mbuf.h"

#include "findSharedMemory.h"


///  Search shared memory device file names in /rtl_mem_*
///	@param[in] *sys_name	Name of system, required to attach shared memory.
///	@return			Pointer to start of shared memory segment for this system.
volatile void *
findSharedMemory(char *sys_name)
{
    int ss_mb = 64;
    if (!strcmp(sys_name, "ipc")) ss_mb = 32;
    if (!strcmp(sys_name, "shmipc")) ss_mb = 16;

    return findSharedMemorySize(sys_name, ss_mb);
}

volatile void *
findSharedMemorySize(char *sys_name, int size_mb)
{
    volatile unsigned char *addr = 0;
    char *s;
    int fd;
    char sys[128];
    strcpy(sys, sys_name);
    for(s = sys; *s; s++) *s=tolower(*s);


    int size_bytes = size_mb*1024*1024;
    printf("Making mbuff area %s with size %d\n", sys, size_bytes);

    if ((fd = open ("/dev/mbuf", O_RDWR | O_SYNC)) < 0) {
        fprintf(stderr, "Couldn't open /dev/mbuf read/write\n");
        return 0;
    }

    struct mbuf_request_struct req;
    req.size = size_bytes;
    strcpy(req.name, sys);
    ioctl (fd, IOCTL_MBUF_ALLOCATE, &req);
    ioctl (fd, IOCTL_MBUF_INFO, &req);

    addr = (volatile unsigned char *)mmap(0, size_bytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        printf("return was %d\n", errno);
        perror("mmap");
        _exit(-1);
    }
    printf(" %s mmapped address is 0x%lx\n", sys, (long)addr);
    return addr;
}

