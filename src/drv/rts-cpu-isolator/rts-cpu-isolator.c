#include <linux/init.h>
#include <linux/module.h>
#include <linux/cpu.h>
#include "rts-cpu-isolator.h"

MODULE_LICENSE( "Dual BSD/GPL" );

play_dead_handler_fp_t original_play_dead_handler = NULL;

static DEFINE_PER_CPU( play_dead_handler_fp_t , fe_code );

int is_cpu_occupied( unsigned int cpu )
{
    return 0 != per_cpu( fe_code, cpu );
}
EXPORT_SYMBOL( is_cpu_occupied );

int
is_cpu_taken_by_rcg_model( unsigned int cpu )
{
    return is_cpu_occupied( cpu );
}
EXPORT_SYMBOL( is_cpu_taken_by_rcg_model );

void set_rt_callback( play_dead_handler_fp_t ptr, unsigned int cpu )
{
    per_cpu( fe_code, cpu ) = ptr;
}
EXPORT_SYMBOL( set_rt_callback );

void
set_fe_code_idle( play_dead_handler_fp_t ptr, unsigned int cpu )
{
    set_rt_callback(ptr, cpu);
}
EXPORT_SYMBOL( set_fe_code_idle );

static void
ligo_play_dead( void )
{
    play_dead_handler_fp_t handler = per_cpu( fe_code, smp_processor_id( ) );
    printk( KERN_ALERT "entering ligo_play_dead, cpu handler = %p", handler );

    if ( handler )
    {
        printk( KERN_ALERT "calling LIGO code" );
        local_irq_disable( );
        ( *handler )( );
        printk( KERN_ALERT "LIGO code is done, calling regular shutdown code" );
        ( *original_play_dead_handler )( );
    }
    else
    {
        ( *original_play_dead_handler )( );
    }
}

static int
rts_cpu_isolator_init( void )
{
    original_play_dead_handler = smp_ops.play_dead;
    printk( KERN_ALERT "rts_cpu_isolator_init.  Handler was %p\n", smp_ops.play_dead );
    smp_ops.play_dead = ligo_play_dead;
    printk( KERN_ALERT "Handler is now %p", smp_ops.play_dead );
    return 0;
}

static void
rts_cpu_isolator_exit( void )
{
    smp_ops.play_dead = original_play_dead_handler;
    printk( KERN_ALERT "rts_cpu_isolator_exit.  Handler restored to %p\n",
            smp_ops.play_dead );
}

module_init( rts_cpu_isolator_init );
module_exit( rts_cpu_isolator_exit );
