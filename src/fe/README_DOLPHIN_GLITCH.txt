1) Dolphin glitch can last up to 64msec.

- iop_read_adc()
    - Will detect and report any long cycle > 500 usec ie return > 0
    - Will detect and report any short cycle < 10 usec ie return -1
        - Indicates data backed up in ADC module FIFO
    - Otherwise report a normal cycle ie return 0;

- Iop code will:
    - On long cycle, stop writing DAC outputs
        - Sets stop_dac_write flag that will temporarily hold all DAC outputs to last value
    - On short cycle, do nothing
    - if normal cycle reported and stop_dac_write flag set:
        - Unset stop_dac_write flag and continue processing DAC outputs.

Works fine for IOP, except maybe add an extra DAC write on return to normal?? 
    - Use dac_preload function??
    - Need to stop DAC FIFO checks during catch up time?

Problem: On return to normal, user apps haven't caught up, IOP will send 0 for their DAC outputs.

Solution:
    - Send flag to user apps to have them simply read ADC data until caught up?
        - This would let them catch up quickly
        - Put this in io_mem_data area and read by app_adc_read()?
    - Have IOP continue to send last value until user apps catch up?
    - Method for IOP to know where user apps are ie cycle count as they try to catch up?
        - Would be complicated algorithm to track every DAC channel
