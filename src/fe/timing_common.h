
#ifndef LIGO_TIMING_COMMON_H
#define LIGO_TIMING_COMMON_H

//***********************************************************************
/// \brief Calculate ADC/DAC duotone offset for diagnostics. \n
///< Code should only run on IOP
//***********************************************************************
#ifdef IOP_MODEL
inline float
duotime( int count, float meanVal, float data[] )
{
    float x, y, sumX, sumY, sumXX, sumXY, msumX;
    int   ii;
    float xInc;
    float offset, slope, answer;
    float den;

    x = 0;
    sumX = 0;
    sumY = 0;
    sumXX = 0;
    sumXY = 0;
    xInc = 1000000 / IOP_IO_RATE;

    for ( ii = 0; ii < count; ii++ )
    {
        y = data[ ii ];
        sumX += x;
        sumY += y;
        sumXX += x * x;
        sumXY += x * y;
        x += xInc;
    }
    msumX = sumX * -1;
    den = ( count * sumXX - sumX * sumX );
    if ( den == 0.0 )
    {
        return ( -1000 );
    }
    offset = ( msumX * sumXY + sumXX * sumY ) / den;
    slope = ( msumX * sumY + count * sumXY ) / den;
    if ( slope == 0.0 )
    {
        return ( -1000 );
    }
    meanVal -= offset;
    answer = meanVal / slope - 91.552;
    return ( answer );
}
inline void
initializeDuotoneDiags( duotone_diag_t* dt_diag )
{
    int ii;
    for ( ii = 0; ii < IOP_IO_RATE; ii++ )
    {
        dt_diag->adc[ ii ] = 0;
        dt_diag->dac[ ii ] = 0;
    }
    dt_diag->totalAdc = 0.0;
    dt_diag->totalDac = 0.0;
    dt_diag->meanAdc = 0.0;
    dt_diag->meanDac = 0.0;
    dt_diag->dacDuoEnable = 0.0;
}
#endif

inline void
initializeTimingDiags( timing_diag_t* timeinfo )
{
    timeinfo->cpuTimeEverMax = 0;
    timeinfo->cpuTimeEverMaxWhen = 0;
    timeinfo->startGpsTime = 0;
    timeinfo->usrTimeMax = 0;
    timeinfo->cycleTimeSec = 0;
    timeinfo->cycleTimeSecLast = 0;
    timeinfo->longestCycle = 0;
    timeinfo->timeHoldWhenHold = 0;
    timeinfo->usrTime = 0;
    timeinfo->cycleTime = 0;
}
inline void
sendTimingDiags2Epics( volatile CDS_EPICS* pLocalEpics,
                       timing_diag_t*      timeinfo,
                       adcInfo_t*          adcinfo )
{
    pLocalEpics->epicsOutput.cpuMeterAvg =  (timeinfo->cycleTimeAvg * UNDERSAMPLE / CYCLE_PER_SECOND);
    pLocalEpics->epicsOutput.cpuMeter = timeinfo->cycleTimeSec;
    pLocalEpics->epicsOutput.cpuMeterMax = timeinfo->cycleTimeMax;
    pLocalEpics->epicsOutput.cpuLongCycle = timeinfo->longestCycle;
    pLocalEpics->epicsOutput.userTime = timeinfo->usrTimeMax;
    timeinfo->cycleTimeSecLast = timeinfo->cycleTimeSec;
    timeinfo->cycleTimeSec = 0;
    timeinfo->timeHoldWhenHold = timeinfo->longestCycle;
    timeinfo->usrTimeMax = 0;
    timeinfo->cycleTimeAvg = 0;
    timeinfo->longestCycle = 0;

    pLocalEpics->epicsOutput.adcWaitTime =
        adcinfo->adcHoldTimeAvg / ( CYCLE_PER_SECOND / UNDERSAMPLE );
    pLocalEpics->epicsOutput.adcWaitMin = adcinfo->adcHoldTimeMin;
    pLocalEpics->epicsOutput.adcWaitMax = adcinfo->adcHoldTimeMax;

    adcinfo->adcHoldTimeAvgPerSec = adcinfo->adcHoldTimeAvg / CYCLE_PER_SECOND;
    adcinfo->adcHoldTimeMax = 0;
    adcinfo->adcHoldTimeMin = 0xffff;
    adcinfo->adcHoldTimeAvg = 0;
}
inline void
captureEocTiming( int            cycle,
                  unsigned int   cycle_gps,
                  timing_diag_t* timeinfo,
                  adcInfo_t*     adcinfo )
{

    // Hold the max cycle time over the last 1 second
    if ( timeinfo->cycleTime > timeinfo->cycleTimeSec )
    {
        timeinfo->cycleTimeSec = timeinfo->cycleTime;
        timeinfo->longestCycle = cycle;
    }
    timeinfo->cycleTimeAvg += timeinfo->cycleTime;
    // Hold the max cycle time since last diag reset
    if ( timeinfo->cycleTime > timeinfo->cycleTimeMax )
        timeinfo->cycleTimeMax = timeinfo->cycleTime;
    // Avoid calculating the max hold time for the first few seconds
    if ( ( timeinfo->startGpsTime + 3 ) < cycle_gps )
    {
        if ( adcinfo->adcHoldTime > adcinfo->adcHoldTimeMax )
            adcinfo->adcHoldTimeMax = adcinfo->adcHoldTime;
        if ( adcinfo->adcHoldTime < adcinfo->adcHoldTimeMin )
            adcinfo->adcHoldTimeMin = adcinfo->adcHoldTime;
        adcinfo->adcHoldTimeAvg += adcinfo->adcHoldTime;
        if ( adcinfo->adcHoldTimeMax > adcinfo->adcHoldTimeEverMax )
        {
            adcinfo->adcHoldTimeEverMax = adcinfo->adcHoldTimeMax;
            adcinfo->adcHoldTimeEverMaxWhen = cycle_gps;
        }
        if ( timeinfo->cycleTimeMax > timeinfo->cpuTimeEverMax )
        {
            timeinfo->cpuTimeEverMax = timeinfo->cycleTimeMax;
            timeinfo->cpuTimeEverMaxWhen = cycle_gps;
        }
    }
}


#endif
