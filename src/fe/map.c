///	\file map.c
///	\brief This file contains the software to find PCIe devices on the bus.

#include <linux/types.h>
#include <linux/kernel.h>
#undef printf
#include <linux/pci.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/delay.h>
#define printf printk
#include <drv/cdsHardware.h>
#include <drv/plx_9056.c>
#include <drv/map.h>
#include <commData3.h>

// Include driver code for all supported I/O cards
#include <drv/gsc16ai64.c>
#include <drv/gsc18ai32.c>
#include <drv/gsc18ai64.c>
#include <drv/gsc_adc_common.c>
#include <drv/gsc16ao16.c>
#include <drv/gsc18ao8.c>
#include <drv/gsc20ao8.c>
#include <drv/gsc_dac_common.c>
#include <drv/accesIIRO8.c>
#include <drv/accesIIRO16.c>
#include <drv/accesDio24.c>
#include <drv/contec6464.c>
#include <drv/contec1616.c>
#include <drv/contec32o.c>
#include <drv/vmic5565.c>
#include <drv/symmetricomGps.c>
#include <drv/spectracomGPS.c>
#include <drv/ligoPcieTiming.c>
#include "map_cards_2_slots.c"

// *****************************************************************************
/// \brief Patch to properly handle PEX PCIe chip for newer (PCIe) General
/// Standards
///< DAC modules ie those that are integrated PCIe boards vs. earlier versions
///< built with carrier boards. \n This is extracted from code provided by GSC..
// *****************************************************************************
void
set_8111_prefetch( struct pci_dev* dacdev )
{
    struct pci_dev* dev = dacdev->bus->self;

    printk( "set_8111_prefetch: subsys=0x%x; vendor=0x%x\n",
            dev->device,
            dev->vendor );
    if ( ( dev->device == 0x8111 ) && ( dev->vendor == PLX_VID ) )
    {
        unsigned int reg;
        // Handle PEX 8111 setup, enable prefetch, set pref size to 64
        // These numbers come from reverse engineering the GSC pxe8111 driver
        // and using their prefetch program to enable the prefetch and set pref
        // size to 64
        pci_write_config_dword( dev, 132, 72 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg );
        pci_write_config_dword( dev, 132, 72 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg | 1 );
        pci_write_config_dword( dev, 132, 12 );
        pci_read_config_dword( dev, 136, &reg );
        pci_write_config_dword( dev, 136, reg | 0x8000000 );
    }
}

// *****************************************************************************
/// Routine to find backplane slot for ADC/DAC cards.
// *****************************************************************************
int 
find_card_slot( CDS_HARDWARE* pCds, int ctype, int cinstance )
{
    int ii,jj;
    for (ii=0;ii<pCds->ioc_cards;ii++)
    {
        if(pCds->ioc_config[ ii ] == ctype && pCds->ioc_instance[ ii ] == cinstance)
            return ii;
    }
    return -1;
}
// *****************************************************************************
/// Routine to find PCI modules and call the appropriate driver initialization
/// software.
// *****************************************************************************
int
mapPciModules( CDS_HARDWARE* pCds )
{
    static struct pci_dev* dacdev;
    int                    status;
    int                    i;
    int                    modCount = 0;
    int fast_adc_cnt = 0;
    int adc_750_cnt = 0;
    int adc_cnt = 0;
    int dac_cnt = 0;
    int dac_18bit_cnt = 0;
    int dac_20bit_cnt = 0;
    int bo_cnt = 0;
    int use_it;

    dacdev = NULL;
    status = 0;

   modCount = map_cards_2_slots ( &cdsPciModules ) ;

    // Map and Initialize ADC and DAC modules
    // This section map cards by model definition order
    for ( i = 0; i < pCds->cards; i++ )
    {
        adc_cnt = 0;
        fast_adc_cnt = 0;
        adc_750_cnt = 0;
        dac_cnt = 0;
        dac_18bit_cnt = 0;
        dac_20bit_cnt = 0;
        // Search system for any module with PLX-9056 and PLX id
        while ( ( dacdev = pci_get_device( PLX_VID, PLX_TID, dacdev ) ) )
        {
            // Check if it is an ADC module
            if ( ( dacdev->subsystem_device == ADC_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == adc_cnt &&
                     pCds->cards_used[ i ].type == GSC_16AI64SSA )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_16AI64SSA, adc_cnt );
                    status = gsc16ai64Init( pCds, dacdev );
                    pCds->adcTimeShift[ adc_cnt ] = pCds->cards_used[i].time_shift;
                    printk( "adc card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        printk( "Map fault GSC16AI64 ADC number %d slot %d\n",
                            adc_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                adc_cnt++;
            }
            // Check if it is a 1M ADC module
            if ( ( dacdev->subsystem_device == ADC_18AI32_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == fast_adc_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AI32SSC1M )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_18AI32SSC1M, fast_adc_cnt );
                    status = gsc18ai32Init( pCds, dacdev );
                    pCds->adcTimeShift[ adc_cnt ] = pCds->cards_used[i].time_shift;
                    printk( "fast adc card on bus %x; device %x\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ) );
                    if(status != 0)
                    {
                        printk( "Map fault GSC18AI32 ADC number %d slot %d\n",
                            fast_adc_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                fast_adc_cnt++;
            }

            // Check if it is a 750KHz ADC module
            if ( ( dacdev->subsystem_device == ADC_18AI64_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == adc_750_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AI64SSC )
                {
                    pCds->adcSlot[ pCds->adcCount] = find_card_slot( pCds, GSC_18AI64SSC, adc_750_cnt );
                    status = gsc18ai64Init( pCds, dacdev );
                    pCds->adcTimeShift[ adc_cnt ] = pCds->cards_used[i].time_shift;
                    printk( "750KHz adc card on bus %x; device %x\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ) );
                    if(status != 0)
                    {
                        printk( "Map fault GSC18AI64 ADC number %d slot %d\n",
                            adc_750_cnt,pCds->adcSlot[ pCds->adcCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                adc_750_cnt++;
            }


            // Search for DAC16 cards
            // Search system for any module with PLX-9056 and PLX id
            // Check if it is a DAC16 module
            if ( ( dacdev->subsystem_device == DAC_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_cnt &&
                     pCds->cards_used[ i ].type == GSC_16AO16 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_16AO16, dac_cnt );
                    status = gsc16ao16Init( pCds, dacdev );
                    printk( "16 bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        printk( "Map fault GSC16AO16 DAC number %d slot %d\n",
                            dac_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                dac_cnt++;
            }

            // Search system for any module with PLX-9056 and PLX id
            // Check if it is a DAC16 module
            if ( ( dacdev->subsystem_device == DAC_18BIT_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_18bit_cnt &&
                     pCds->cards_used[ i ].type == GSC_18AO8 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_18AO8, dac_18bit_cnt );
                    status = gsc18ao8Init( pCds, dacdev );
                    printk( "18-bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        printk( "Map fault GSC18AO8 DAC number %d slot %d\n",
                            dac_18bit_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    }
                }
                dac_18bit_cnt++;
            }

            // Check if it is a DAC20 module
            if ( ( dacdev->subsystem_device == DAC_20BIT_SS_ID ) &&
                 ( dacdev->subsystem_vendor == PLX_VID ) )
            {
                if ( pCds->cards_used[ i ].instance == dac_20bit_cnt &&
                     pCds->cards_used[ i ].type == GSC_20AO8 )
                {
                    pCds->dacSlot[ pCds->dacCount] = find_card_slot( pCds, GSC_20AO8, dac_20bit_cnt );
                    status = gsc20ao8Init( pCds, dacdev );
                    printk( "20-bit dac card on bus %x; device %x status %d\n",
                            dacdev->bus->number,
                            PCI_SLOT( dacdev->devfn ),
                            status );
                    if(status != 0)
                    {
                        printk( "Map fault GSC18AO8 DAC number %d slot %d\n",
                            dac_20bit_cnt,pCds->dacSlot[ pCds->dacCount]);
                        return IO_CARD_MAP_ERROR;
                    } 
                }
                dac_20bit_cnt++;
            }
        } // end of while
    } // end of pci_cards used

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-DIO  modules
    while ( ( dacdev = pci_get_device( ACC_VID, ACC_TID, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_24DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            printk( "Access 24 BIO card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
            status = accesDio24Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-IIRO-8 isolated I/O modules
    while ( ( dacdev = pci_get_device( ACC_VID, PCI_ANY_ID, dacdev ) ) )
    {
        if ( dacdev->device != ACC_IIRO_TID &&
             dacdev->device != ACC_IIRO_TID_OLD )
            continue;
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_8DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            printk( "Access 8 BIO card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
            status = accesIiro8Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }


    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    // Search for ACCESS PCI-IIRO-16 isolated I/O modules
    while ( ( dacdev = pci_get_device( ACC_VID, PCI_ANY_ID, dacdev ) ) )
    {
        if ( dacdev->device != ACC_IIRO_TID16 &&
             dacdev->device != ACC_IIRO_TID16_OLD )
            continue;
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == ACS_16DIO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            printk( "Access BIO-16 card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = accesIiro16Init( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DIO_6464L_PE isolated I/O modules
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DIO_6464L_PE, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == CON_6464DIO &&
                     ( pCds->cards_used[ i ].instance * 2 ) == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            printk( "Contec 6464 DIO card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = contec6464Init( pCds, dacdev );
            modCount++;
            modCount++;
        }
        bo_cnt++;
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DIO_1616L_PE isolated I/O modules
    // Support for this card is strictly limited in use to
    // setting LIGO timing card (non-PCIe version) settings
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DIO_1616L_PE, dacdev ) ) )
    {
        printk( "Contec 1616 DIO card on bus %x; device %x\n",
                dacdev->bus->number,
                PCI_SLOT( dacdev->devfn ) );
        status = contec1616Init( pCds, dacdev );
        modCount++;
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;
    bo_cnt = 0;

    // Search for Contec C_DO_32L_PE isolated I/O modules
    while ( ( dacdev = pci_get_device( CONTEC_VID, C_DO_32L_PE, dacdev ) ) )
    {
        use_it = 0;
        if ( pCds->cards )
        {
            use_it = 0;
            /* See if ought to use this one or not */
            for ( i = 0; i < pCds->cards; i++ )
            {
                if ( pCds->cards_used[ i ].type == CON_32DO &&
                     pCds->cards_used[ i ].instance == bo_cnt )
                {
                    use_it = 1;
                    break;
                }
            }
        }
        if ( use_it )
        {
            printk( "Contec BO card on bus %x; device %x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ) );
            status = contec32OutInit( pCds, dacdev );
            modCount++;
        }
        bo_cnt++;
    }

    dacdev = NULL;
    status = 0;

    for ( i = 0; i < MAX_RFM_MODULES; i++ )
    {
        pCds->pci_rfm[ i ] = 0;
    }

    dacdev = NULL;
    status = 0;
    pCds->gps = 0;
    pCds->gpsType = 0;
    dacdev = NULL;
    status = 0;
    // Look for TSYNC GPS board
    if ( ( dacdev = pci_get_device( TSYNC_VID, TSYNC_TID, dacdev ) ) )
    {
        printk( "TSYNC GPS card on bus %x; device %x\n",
                dacdev->bus->number,
                PCI_SLOT( dacdev->devfn ) );
        status = spectracomGpsInit( pCds, dacdev );
    }
    // Look for a LIGO PCIe Timing Card
    dacdev = NULL;
    status = 0;
    bo_cnt = 0;
    while ( ( dacdev = pci_get_device( 0x10ee, PCI_ANY_ID, dacdev ) ) )
    {
	if(dacdev != NULL) {
            printk( "Xilinx card on bus %x; device %x vendor 0x%x\n",
                    dacdev->bus->number,
                    PCI_SLOT( dacdev->devfn ),
                    dacdev->device );
            status = lptcInit( pCds, dacdev );
	}
    }


    return ( modCount );
}
