/// @file rcguserCommon.c
/// @brief File contains routines for user app startup

#include "print_io_info.c"

// **********************************************************************************************
// Capture SIGHALT from ControlC
void
intHandler( int dummy )
{
    pLocalEpics->epicsInput.vmeReset = 1;
}

// **********************************************************************************************

void
attach_shared_memory( char* sysname )
{

    char         shm_name[ 64 ];

    // epics shm used to communicate with model's EPICS process
    sprintf( shm_name, "%s", sysname );
    _epics_shm = (char*) findSharedMemory( sysname );
    if ( _epics_shm == NULL )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _epics_shm );
        return -1;
    }
    printf( "EPICSM at 0x%lx\n", (long)_epics_shm );

    // testpoint config shm used to control testpoints from awgtpman
    sprintf( shm_name, "%s%s", sysname, SHMEM_TESTPOINT_SUFFIX );
    _tp_shm = (volatile TESTPOINT_CFG *) findSharedMemory( sysname );
    if ( _tp_shm == NULL )
    {
        printf( "mbuf_allocate_area(tp) failed; ret = %d\n", _tp_shm );
        return -1;
    }
    printf( "TPSM at 0x%lx\n", (long)_tp_shm );
    
    // awg data shm used to stream data from awgtpman
    sprintf( shm_name, "%s%s", sysname, SHMEM_AWG_SUFFIX );
    _awg_shm = (volatile AWG_DATA *) findSharedMemory( sysname );
    if ( _awg_shm == NULL )
    {
        printf( "mbuf_allocate_area(awg) failed; ret = %d\n", _awg_shm );
        return -1;
    }
    printf( "AWGSM at 0x%lx\n", (long)_awg_shm );
    
    // ipc_shm used to communicate with IOP
    _ipc_shm = (char*)findSharedMemory( "ipc" );
    if ( _ipc_shm == NULL )
    {
        printf( "mbuf_allocate_area(ipc) failed; ret = %d\n", _ipc_shm );
        return -1;
    }
    printf( "IPC    at 0x%lx\n", (long)_ipc_shm );
    ioMemData = (volatile IO_MEM_DATA*)( ( (char*)_ipc_shm ) + 0x4000 );
    printf(
        "IOMEM  at 0x%lx size 0x%x\n", (long)ioMemData, sizeof( IO_MEM_DATA ) );
    printf( "%d PCI cards found\n", ioMemData->totalCards );

    // DAQ is via shared memory
    sprintf( shm_name, "%s_daq", sysname );
    _daq_shm = (char*) findSharedMemory( shm_name );
    if ( _daq_shm == NULL )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _daq_shm );
        return -1;
    }
    printf( "DAQSM at 0x%lx\n", _daq_shm );
    daqPtr = (struct rmIpcStr*)_daq_shm;

    // shmipc is used to send SHMEM IPC data between processes on same computer
    _shmipc_shm = (char*) findSharedMemory( "shmipc" );
    if ( _shmipc_shm == NULL )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _shmipc_shm );
        return -1;
    }

    // Open new IO shared memory in support of no hardware I/O
    _io_shm = (char*) findSharedMemory( "virtual_io_space" );
    if ( _io_shm == NULL )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _io_shm );
        return -1;
    }
    ioMemDataIop = (volatile IO_MEM_DATA_IOP*)( ( (char*)_io_shm ) );
}

