#!/usr/bin/env python2
# Test report generation routines

from subprocess import call
from epics import PV
from epics import caget, caput
from math import sin,cos,radians
import sys
import time
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
sys.path.insert(0,path.join(rcg_dir, 'test/rcgtest/python'))
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM


tr = testreport('biotest','BIO I/O Test',112)
testword = "X2:IOP-SAM_TEST_STAT_3"
testwordepics = PV(testword)
testwordepics.value = 2
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify IOP passes proper BIO module mapping to user models. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = 'This test writes to two (2) Contec6464 modules to verify correct mapping \n'
s += 'to user application models. It does this by writing to the output registers \n'
s += 'from two user models and then reads back the output register for proper settings. \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = 'This test was designed to run on the LLO DTS on x2ats:\n'
s += '  - Models x2iopsam, x2atstim32, x2atstim16, x2atstim04 and x2atstim02 \n'
s += '    must be running on x2ats. \n'
s += '  - Models x2atstim04 and x2atstim16 must be compiled with the biotest=1 option  \n'
tr.sectiontext(s)


tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/rtscore/advligorts/test/rcgtest/medm/BIOTEST.adl \n'
s += 'This screen provides basic capability to enter and readback BIO settings. \n'
tr.sectiontext(s)
tr.image('biotest')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = 'The basic procedure is to write various values to the BIO cards and read '
s += 'back the settings and verify proper setting was set to designeted cards'
tr.sectiontext(s)

pdiag = dtt()
# Start the test ******************************************

ii = 0;

tr.teststate.value = 'BIO Module Test'

testvals = [1.0,2.0,3.0,4.0]
# List is 18_0, 20_0, 16_0 , 18_1, 18_2
write_chans = ['X2:ATS-TIM04_BIO_DECODE_DIO_UPPER_OUT','X2:ATS-TIM04_BIO_DECODE1_DIO_UPPER_OUT',
           'X2:ATS-TIM16_BIO_ENCODE_TEST_OUT','X2:ATS-TIM16_BIO_DECODE1_DIO_UPPER_OUT',
           'X2:ATS-TIM16_BIO_ENCODE_TEST_SW1' ]

read_chans = ['X2:ATS-TIM04_BIO_DECODE_DIO_UPPER_IN','X2:ATS-TIM04_BIO_DECODE1_DIO_UPPER_IN',
            'X2:ATS-TIM16_BIO_DECODE_DIO_0_IN','X2:ATS-TIM16_BIO_DECODE1_DIO_UPPER_IN']


tr.teststatus == 0
#STEP 1 ***************************************************************************************************
tr.sectionhead('Step 1: Initialize BIO Settings')
tr.teststeptxt = '\tSet all outputs to zero\n'
tr.teststeptxt += '\tReadback board registers and verify set to zero\n'
# Clear all BIO outputs *************************************************
for ii in range (0,4):
    testw = PV(write_chans[ii])
    testw.value = 10
time.sleep(2)
for ii in range (0,4):
    testr = PV(read_chans[ii])
    tr.teststeptxt += 'value = ' + repr(testr.value) + '\n'
time.sleep(2)
for ii in range (0,4):
    testw = PV(write_chans[ii])
    testw.value = 0
time.sleep(2)
for ii in range (0,4):
    testr = PV(read_chans[ii])
    tr.teststeptxt += 'value = ' + repr(testr.value) + '\n'
tr.sectionstep()

#STEP 2 *******************************************************************************
tr.sectionhead('Step 2: Verify Channel Mapping')
tr.teststeptxt = '\tSet various values and verify readback values:\n'
kk = 0
for ii in range (0,4):
    testw = PV(write_chans[ii])
    if kk == ii:
        testw.value = 11234
    else:
        testw.value = 1
    kk += 1
    time.sleep(2)
    testv = []
    for jj in range (0,4):
        testr = PV(read_chans[jj])
        testv.append(testr.value)
    tr.teststeptxt += 'value = ' + repr(testv[0]) + '\t' + repr(testv[1]) +  '\t' + repr(testv[2]) +  '\t' +  repr(testv[3]) + '\n'
tr.sectionstep()

if(tr.teststatus == 0):
    tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
tr.sectionstep()


testwordepics.value = 0
if tr.teststatus == 0:
    testwordepics.value = 1


tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/tmp/rcgtest/data/biotest.dox"])
sys.exit(tr.teststatus)

