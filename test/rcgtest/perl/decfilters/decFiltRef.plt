set term png
unset logscale; set logscale x
set title "IOP Decimation Filter Reference Data"
set key left bottom
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude (db)'
set xrange [100:20000]
#set xtics (100,1000,10000,20000)
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100
set grid mxtics ls 101
set output "/tmp/rcgtest/images/decFiltRef.png"
plot "/tmp/rcgtest/tmp/ref1.data" using 1:4 smooth unique title "2K Dec" , "/tmp/rcgtest/tmp/ref1.data" using 1:6 smooth unique title "4k Dec", "/tmp/rcgtest/tmp/ref1.data" using 1:8 smooth unique title "16K Dec", "/tmp/rcgtest/tmp/ref1.data" using 1:10 smooth unique title "32K Dec"
