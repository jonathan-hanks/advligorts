#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq
##
##
use threads;
use threads::shared;
BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $rcgdir = $ENV{'RCG_DIR'};
        $epicsperl = $epicsbase . "/lib/perl";
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
	push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"$rcgdir/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"$rcgdir/src/perldaq";
}
	
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";

my $test_summary = "FAIL";

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);
my $ii = 0;
my $status = 0;

print  "\n\nSTARTING DECIMATION FILTER MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n";


caPut("X2:IOP-SAM_TEST_STATUS","DECIMATION FILTER TEST ");
caPut("X2:IOP-SAM_TEST_STAT_4",2);

# Setup the Decimation Filters for test *************
caSwitch("X2:IOP-SAM_DEC_FILTER_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
caSwitch("X2:IOP-SAM_DEC_FILTER_32K","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:IOP-SAM_DEC_FILTER_16K","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:IOP-SAM_DEC_FILTER_4K","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:IOP-SAM_DEC_FILTER_2K","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");

$runfile = $rcgdir . "/test/rcgtest/perl/decfilters/dftrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/dftrunfile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/decfilters/Template_Dec_Filters.xml";
$results_file = $rcgtesttmpdir . "/dftdata.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/ref1.data";
$test_data_file = $rcgtesttmpdir . "/dft.data";
my $cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);
#convert .xml files to .data files.
#system ('xmlconv -m Template_Dec_Filters.xml ref1.data "Result[0]"');
#system ('xmlconv -m dftdata.xml dft.data "Result[0]"');

#Read xfer function frequency data
(my $testPts,my @freq) = dtReadDataFile($test_data_file,0);

#Read xfer function data for 2k filter
($testPts,my @data2k) = dtReadDataFile($test_data_file,3);
($testPts,my @data2kR) = dtReadDataFile($ref_data_file,3);
my $result2k = "PASS";
my $spec2k = 0.05;
(my $errs, my @diff) = dtCheckData($testPts,$spec2k,@data2k,@data2kR);
print "2K errors = $errs \n";
if($errs) { $result2k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 4k filter
($testPts,my @data4k) = dtReadDataFile($test_data_file,5);
($testPts,my @data4kR) = dtReadDataFile($ref_data_file,5);
my $result4k = "PASS";
my $spec4k = 0.05;
($errs, @diff) = dtCheckData($testPts,$spec4k,@data4k,@data4kR);
print "4K errors = $errs  \n";
if($errs) { $result4k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 16k filter
($testPts,my @data16k) = dtReadDataFile($test_data_file,7);
($testPts,my @data16kR) = dtReadDataFile($ref_data_file,7);
my $result16k = "PASS";
my $spec16k = 0.3;
($errs, @diff) = dtCheckData($testPts,$spec16k,@data16k,@data16kR);
print "16K errors = $errs  \n";
if($errs) { $result16k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 32k filter
($testPts,my @data32k) = dtReadDataFile($test_data_file,9);
($testPts,my @data32kR) = dtReadDataFile($ref_data_file,9);
my $result32k = "PASS";
my $spec32k = 0.1;
($errs, @diff) = dtCheckData($testPts,$spec32k,@data32k,@data32kR);
print "32K errors = $errs  \n";
if($errs) { $result32k = "FAIL"; }
if($errs) { $status = -1; }

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


#Make data plots
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/decfilters/decFiltTest.plt");
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/decfilters/decFiltRef.plt");

# Generate Test Report
$testdatafile = $rcgtestdatadir . "/decFiltTestData.dox";
open(OUTM,'>', $testdatafile) || die "cannot open test data file for writing";

print OUTM <<END;
/*!     \\page decFiltTest IOP Decimation Filter Test Data
*       \\verbatim
*************************************************************************************************

IOP DECIMATION FILTER CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************
PURPOSE: Verify the operation of the ADC decimation filters, as defined in controller.c .

OVERVIEW: 
	The IOP code runs at a native 64K sampling rate. It delivers ADC data to and receives DAC
	data from user application models, which typically run at lower rates.. In order to 
	interface this data, up/down sampling DAC/ADC IIR filters are provided by use in application 
	models running at 32K, 16K, 4K, or 2K. These up/down sampling filters hardcoded into the 
	controller.c file.

TEST REQUIREMENTS:
This script (decFiltTest.pl) was designed to run on the LLO DAQ test system. It requires that the x2ats
computer is running the x2atstim16.mdl software. 
There are five filter modules built into the h1iopfe3.mdl for use in running this test:
	- X2IOP_SAM_DEC_FILTER_TEST: This filter is used to inject the test signal. Its output is 
	  connected to each of the following four test filters.
	- X2IOP_SAM_DEC_FILTER_2K: Loaded with the decimation filter defined when using a 2K app model.
	- X2IOP_SAM_DEC_FILTER_4K: Loaded with the decimation filter defined when using a 4K app model.
	- X2IOP_SAM_DEC_FILTER_16K: Loaded with the decimation filter defined when using a 16K app model.
	- X2IOP_SAM_DEC_FILTER_32K: Loaded with the decimation filter defined when using a 32K app model.

TEST PROCEDURE:
	- Setup the test filter module switches. 
	- Run diag tool based on a reference template file.
	- Read xfer function data and compare with reference data from template file.
	- Plot reference data and test data.


        \\endverbatim
*       \\verbatim
DTT Test Template File.
        \\endverbatim

*       \\verbatim

TEST RESULTS:

 2K Filter	- 	$result2k
 4K Filter	- 	$result4k
16K Filter	- 	$result16k
32K Filter	- 	$result32k


TEST SUMMARY:  $test_summary

        \\endverbatim

\\image html decFiltRef.png 
\\image latex decFiltRef.png 
*       \\verbatim


        \\endverbatim
\\image html decFiltTest.png 
\\image latex decFiltTest.png 

*/
END
close OUTM;
print "TEST COMPLETE ***********************************************************************************\n";
caPut("X2:IOP-SAM_TEST_STAT_4",1);
if($status < 0) {
    caPut("X2:IOP-SAM_TEST_STAT_4",0);
}

caPut("X2:IOP-SAM_TEST_STATUS","SYSTEM IDLE ");


exit($status);

