#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq
##
##
use threads;
use threads::shared;
BEGIN {
	 $epicsbase = $ENV{'EPICS_BASE'};
         $epicslib = $epicsbase . "/lib/linux-x86_64";
         $epicsperl = $epicsbase . "/lib/perl";
        $rcgtestdir = $ENV{'RCG_TEST_DIR'};
        $rcgdir = $ENV{'RCG_DIR'};
         print "EPICSBASE = $epicsbase \n";
         print "EPICSLIB = $epicslib \n";
         print "EPICSPERL = $epicsperl \n";
         push @INC,"/opt/cdscfg/tst/stddir.pl";
         push @INC,"$epicsperl";
         push @INC,"$epicslib";
        push @INC,"/opt/rtapps/debian10/perlmodules";
    push @INC,"$rcgdir/test/rcgtest/perl";
	push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
	push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
	push @INC,"$rcgdir/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;

$rcgtestdir = $ARGV[0];
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";

my $test_summary = "FAIL";
# Need daqperl module to get data from NDS
require "daq.pm";


(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_BUILD_SVN","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);
my $ii = 0;
my $status = 0;

print  "\n\nSTARTING STANDARD FILTER MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n";
print "SVN Version: $strEpicsVals[0]\n\n";


caPut("X2:IOP-SAM_TEST_STATUS","SFM TEST - RAMP FUNCTION");
caPut("X2:IOP-SAM_TEST_STAT_13",2);



# Start filter module ramp timing test ******************************************************************
caSwitch("X2:ATS-TIM16_T2_ADC_FILTER_2","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");

caPut("X2:ATS-TIM16_T2_ADC_FILTER_2_OFFSET",2000);
caPut("X2:ATS-TIM16_T2_ADC_FILTER_2_TRAMP",0.5);

my $daqChan = "X2:ATS-TIM16_T2_ADC_FILTER_2_OUT_DQ";

sleep(4);
# get the channel list
DAQ::connect("localhost", 8088);

# get current time
$gps = DAQ::gps();

caSwitch("X2:ATS-TIM16_T2_ADC_FILTER_2","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET ON OUTPUT ON");

# want to get data in past from frames
print "gps time is $gps \n";
sleep(64);
$gps += 1;

print "Getting data from NDS \n";
# Get data from NDS for 3 second period, starting on 1 second boundary
my @rampData = DAQ::acquire($daqChan, 3, $gps);

my $arrSize = @rampData;
print "Array size = $arrSize \n";
if($arrSize < 2000) {
	print "NO DATA \n";
	exit (-1);
}

my $start = 0;
my $stPt = 0;
my $endPt = 0;
my $kk = 0;

$test_data_file = $rcgtesttmpdir . "/ramp.data";
open(DATAFILE,'>', $test_data_file) || die "cannot open test data file for writing";
for($ii=0;$ii<48000;$ii++)
{
	if(($rampData[$ii] > 0) && ($stPt == 0))
	{
		$stPt = $ii-1;
		print DATAFILE "$kk	$rampData[$ii-1] \n";
		$kk ++;
	} 
	if(($stPt != 0) && ($endPt == 0))
	{
		print DATAFILE "$kk	$rampData[$ii] \n";
		$kk ++;
	}
	if(($rampData[$ii] == 2000) && ($endPt == 0))
	{
		$endPt = $ii;
	}
	
		
}
close(DATAFILE);

# Put filter switches back to normal
caSwitch("X2:ATS-TIM16_T2_ADC_FILTER_2","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");

#Calc test results for ramping
my $rampCycles = $endPt - $stPt;
my $rampTime = $rampCycles / 16384.0;
my $rampReqLo = 0.48;
my $rampReqHi = 0.51;
my $rampPf = "PASS";

if(($rampTime < $rampReqLo) || ($rampTime > $rampReqHi))
{
	$rampPf = "FAIL";
	$status = -1;
}

print "Ramp time = $rampTime with $rampCycles cycles recorded - $rampPf\n";

# Start test of 200Hz LP filter *************************************************************************
caPut("X2:IOP-SAM_TEST_STATUS","SFM TEST - 200Hz LP");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_11","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
sleep(4);
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_11","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
sleep(4);

print " Starting LP200 Test \n";
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";

$runfile = $rcgdir . "/test/rcgtest/perl/sfm/sfmrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/sfmRunFile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/sfm/Template_FE3_TIM16_T1_ADC_FILTER_11_LP200.xml";
$results_file = $rcgtesttmpdir . "/sfmLP200data.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/sfmLP200reference.data";
$test_data_file = $rcgtesttmpdir . "/sfmLP200.data";

print "Writing test data files $ref_data_file $test_data_file\n";
my $cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);

($testPts,my @testData) = dtReadDataFile($test_data_file,3);
($testPts,my @refData) = dtReadDataFile($ref_data_file,3);
($testPts,my @freq) = dtReadDataFile($ref_data_file,0);
my $reqVal = .18;
(my $errs, my @diff) = dtCheckData($testPts,$reqVal,@testData,@refData);


my $pf200 = "PASS";
if($errs) {
	$pf200 = "FAIL";
	$status = -1;
    print "LP200 errors = $errs \n";
}
print "Finished LP200 Test == $pf200 \n";

# Start of Notch Filter Testing **********************************************************************
print " Starting Notch Test \n";
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
caPut("X2:IOP-SAM_TEST_STATUS","SFM TEST - 256Hz Notch");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_11","FM1 OFF FM2 ON FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
sleep(4);

$runfile = $rcgdir . "/test/rcgtest/perl/sfm/sfmrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/sfmRunFile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/sfm/Template_FE3_TIM16_T1_ADC_FILTER_11_NCH256.xml";
$results_file = $rcgtesttmpdir . "/sfmNotchdata.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/sfmNotchReference.data";
$test_data_file = $rcgtesttmpdir . "/sfmNotch.data";

print "Writing test data files $ref_data_file $test_data_file\n";
$cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);

($testPts,my @testData1) = dtReadDataFile($test_data_file,3);
($testPts,my @refData1) = dtReadDataFile($ref_data_file,3);
($testPts,my @freq1) = dtReadDataFile($ref_data_file,0);
my $reqValNotch = .1;
($errs, my @diff1) = dtCheckData($testPts,$reqValNotch,@testData1,@refData1);

my $pfNotch = "PASS";
if($errs) {
    $pfNotch = "FAIL";
    $status = -1;
}
print "Finished Notch Filter Test == $pfNotch \n";

#Start of 3030 Filter Testing ****************************************************************
print " Starting 3030 Test \n";
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
caPut("X2:IOP-SAM_TEST_STATUS","SFM TEST - 30Hz 30db Gain");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_10","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
sleep(4);

$runfile = $rcgdir . "/test/rcgtest/perl/sfm/sfmrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/sfmRunFile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/sfm/Template_FE3_TIM16_T1_ADC_FILTER_10_3030.xml";
$results_file = $rcgtesttmpdir . "/sfm3030.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/sfm3030Reference.data";
$test_data_file = $rcgtesttmpdir . "/sfm3030.data";

print "Writing test data files $ref_data_file $test_data_file\n";
$cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);

($testPts,my @testData2) = dtReadDataFile($test_data_file,3);
($testPts,my @refData2) = dtReadDataFile($ref_data_file,3);
($testPts,my @freq2) = dtReadDataFile($ref_data_file,0);
my $reqVal3030 = .1;
($errs, my @diff2) = dtCheckData($testPts,$reqVal3030,@testData2,@refData2);

my $pf3030 = "PASS";
if($errs) {
    $pf3030 = "FAIL";
    $status = -1;
}
print "Finished 3030 Filter Test == $pf3030 \n";

#Start of 50Hz Eliptical Filter Testing *************************************************************
print " Starting 50 Eliptical Test \n";
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
caPut("X2:IOP-SAM_TEST_STATUS","SFM TEST - 50Hz Eliptical");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_10","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 ON INPUT OFF OFFSET OFF OUTPUT ON");
sleep(4);

$runfile = $rcgdir . "/test/rcgtest/perl/sfm/sfmrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/sfmRunFile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/sfm/Template_FE3_TIM16_T1_ADC_FILTER_10_EL50.xml";
$results_file = $rcgtesttmpdir . "/sfmEL50.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/sfmEL50Reference.data";
$test_data_file = $rcgtesttmpdir . "/sfmEL50.data";

print "Writing test data files $ref_data_file $test_data_file\n";
$cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);

($testPts,my @testData3) = dtReadDataFile($test_data_file,3);
($testPts,my @refData3) = dtReadDataFile($ref_data_file,3);
($testPts,my @freq3) = dtReadDataFile($ref_data_file,0);
my $reqValEL50 = .1;
($errs, my @diff3) = dtCheckData($testPts,$reqValEL50,@testData3,@refData3);

my $pfEL50 = "PASS";
if($errs) {
    $pfEL50 = "FAIL";
    $status = -1;
}
print "Finished EL50 Filter Test == $pf3030 $errs\n";

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


#Make data plots
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/sfm/sfmTest.plt");

# Generate Test Report
open(OUTM,">/tmp/rcgtest/data/sfmTestData.dox") || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page fmcTest Standard Filter Module Test Data
*       \\verbatim
*************************************************************************************************

STANDARD FILTER MODULE (SFM) CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************
PURPOSE: Verify the operation of RCG standard IIR filter modules.

TEST OVERVIEW:
	This test performs transfer functions with various filters loaded and compares them against
   	template reference files to verify proper filter operation.

TEST REQUIREMENTS:
	This test was designed for use with the LLO DAQ Test Stand. It requires that the DAQ system is
	operational and that computer x2ats is loaded with the x2atstim16 model running.

TEST PROCEDURE:
	This test is automatically run by executing the sfmTest.pl script, which is located in the
	/opt/rtcds/userapps/trunk/cds/test/scripts/filters directory. This directory also contains,
	for each filter definition to be tested:
		- Template files, which contain the test reference (requirements) data. 
		- Shell (.sh) scripts to launch DTT diag sessions.
		- Command (.cmd) files loaded into DTT diag.
	
	Three filter modules are used in this test:
		1) X2:ATS_TIM16_T2_ADC_FILTER_2, used to test ramping time and ramp features.
			1) OFFSET set to 2000
			2) TRAMP set to 0.5 seconds
		2) X2:ATS_TIM16_T1_ADC_FILTER_11, loaded with filters:
			1) 200Hz Low Pass
			2) 256Hz Notch
		3) X2:ATS_TIM16_T1_ADC_FILTER_10, loaded with filters selected from the h1susmc2
		   coefficient file:
			1) 30Hz, 30db gain
			2) 50Hz Eliptical
			3) 1Hz Gain

	For Ramp testing, OFFSET switch set to ON. Data is then collected via NDS to determine ramp
	time and shape (spline). 

	For each filter defined, the test script will:
		1) Turn ON the appropriate filter.
		2) Invoke DTT diag using the defined template file. This will result in a transfer function
		   being performed and output saved as an .xml file.
		3) Run GDS xmlconv command on the test data and template .xml files to produce test data text files.
		4) Read data from both data text files and compare values. If values are within .1db of each other for
		   all frequencies measured, then test is given a PASS. If not, test is marked as "FAIL".


TEST RESULTS:
        \\endverbatim

*       \\verbatim
-------------------------------------------------------------------------------------------------
Test of Ramp Timing			$rampReqLo to $rampReqHi	$rampTime	$rampPF
        \\endverbatim
\\image html sfmRamp.png "Ramping Data for .5sec from 0 to 2000"
\\image latex sfmRamp.png "Ramping Data for .5sec from 0 to 2000"
*       \\verbatim
-------------------------------------------------------------------------------------------------
Test with 200Hz LP Filter -	 	$pf200		
        \\endverbatim
\\image html sfmLP200.png "LP200 Data"
\\image latex sfmLP200.png "LP200 Data"
*       \\verbatim
-------------------------------------------------------------------------------------------------
Test with 256Hz Notch Filter - 		$pfNotch	
        \\endverbatim
\\image html sfmNotch.png "Notch Data"
\\image latex sfmNotch.png "Notch Data"
*       \\verbatim
-------------------------------------------------------------------------------------------------
Test with 30Hz 30db Gain Filter - 	$pf3030	
        \\endverbatim
\\image html sfm3030.png "3030 Data"
\\image latex sfm3030.png "3030 Data"
*       \\verbatim
-------------------------------------------------------------------------------------------------
Test with 50Hz Eliptical Filter - 	$pfEL50	
        \\endverbatim
\\image html sfmEL50.png "EL50 Data"
\\image latex sfmEL50.png "EL50 Data"

TEST SUMMARY:  $test_summary


*       \\verbatim
*************************************************************************************************
200Hz LP TEST DATA
Frequency 	Reference	Test		Difference
*************************************************************************************************
END
for($ii=0;$ii<61;$ii++)
{
	my $errOut = " ";
	if($diff[$ii] > $reqVal) { $errOut = "*"; }
	$freqOut1 = sprintf("%.4f",$freq[$ii]);
	$refOut1 = sprintf("%.6f",$refData[$ii]);
	$testOut1 = sprintf("%.6f",$testData[$ii]);
	$diffOut1 = sprintf("%.6f",$diff[$ii]);
	if($freq[$ii] < 100) {
		print OUTM " $freqOut1	$refOut1	$testOut1	$diffOut1 	$errOut\n";
	}else {
		print OUTM "$freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
}

print OUTM <<END;
        \\endverbatim


*       \\verbatim
*************************************************************************************************
256Hz NOTCH FILTER TEST DATA
Frequency 	Reference	Test		Difference
*************************************************************************************************
END
for($ii=0;$ii<101;$ii++)
{
	$errOut = " ";
	if($diff1[$ii] > $reqValNotch) { $errOut = "*"; }
	$freqOut1 = sprintf("%.4f",$freq1[$ii]);
	$refOut1 = sprintf("%.6f",$refData1[$ii]);
	$testOut1 = sprintf("%.6f",$testData1[$ii]);
	$diffOut1 = sprintf("%.6f",$diff1[$ii]);
	if($freq1[$ii] < 100) {
		print OUTM " $freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}else {
		print OUTM "$freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
}
print OUTM <<END;
        \\endverbatim

*       \\verbatim
*************************************************************************************************
30Hz 30db GAIN FILTER TEST DATA
Frequency 	Reference	Test		Difference
*************************************************************************************************
END

for($ii=0;$ii<61;$ii++)
{
	$errOut = " ";
	if($diff2[$ii] > $reqVal3030) { $errOut = "*"; }
	$freqOut1 = sprintf("%.4f",$freq2[$ii]);
	$refOut1 = sprintf("%.6f",$refData2[$ii]);
	$testOut1 = sprintf("%.6f",$testData2[$ii]);
	$diffOut1 = sprintf("%.6f",$diff2[$ii]);
	if($freq2[$ii] < 10) {
		print OUTM "  $freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
	if(($freq2[$ii] >= 10) && ($freq2[$ii] < 100)) {
		print OUTM " $freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
	if($freq2[$ii] >= 100) {
		print OUTM "$freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
}

print OUTM <<END;
        \\endverbatim

*       \\verbatim
*************************************************************************************************
50Hz ELIPTICAL FILTER TEST DATA
Frequency 	Reference	Test		Difference
*************************************************************************************************
END

for($ii=0;$ii<61;$ii++)
{
	$errOut = " ";
	if($diff3[$ii] > $reqValEL50) { $errOut = "*"; }
	$freqOut1 = sprintf("%.4f",$freq3[$ii]);
	$refOut1 = sprintf("%.6f",$refData3[$ii]);
	$testOut1 = sprintf("%.6f",$testData3[$ii]);
	$diffOut1 = sprintf("%.6f",$diff3[$ii]);
	if($freq3[$ii] < 10) {
		print OUTM "  $freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
	if(($freq3[$ii] >= 10) && ($freq3[$ii] < 100)) {
		print OUTM " $freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
	if($freq3[$ii] >= 100) {
		print OUTM "$freqOut1	$refOut1	$testOut1	$diffOut1	$errOut \n";
	}
}
print OUTM <<END;

TEST SUMMARY:  $test_summary


        \\endverbatim
*\/

END


close OUTM;


print "TEST COMPLETE ***********************************************************************************\n";
caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_11","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_10","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "COMPLETION TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
system("cat /tmp/rcgtest/data/sfmTestData.dox");
print  "COMPLETION TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
caPut("X2:IOP-SAM_TEST_STAT_13",1);
if($status < 0) {
    caPut("X2:IOP-SAM_TEST_STAT_13",0);
}
exit($status);

