#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq
##
##
use threads;
use threads::shared;
BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $epicsperl = $epicsbase . "/lib/perl";
        $rcgdir = $ENV{'RCG_DIR'};
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
        push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"$rcgdir/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"$rcgdir/src/perldaq";

}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";

my $test_summary = "FAIL";

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-110_RCG_VERSION","X2:FEC-110_MSG","X2:FEC-110_MSGDAQ","X2:FEC-110_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);
my $ii = 0;
my $status = 0;

print  "\n\nSTARTING DAQ DECIMATION FILTER MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n\n";

caPut("X2:IOP-SAM_TEST_STAT_5",2);


caPut("X2:IOP-SAM_TEST_STATUS","DAQ DECIMATION FILTER TEST ");

# Setup the Decimation Filters for test *************
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_TST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X2","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X4","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X8","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X16","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X32","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X64","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X128","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");
caSwitch("X2:ATS-TIM16_T1_DAQ_FILTER_X256","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT ON");

$runfile = $rcgdir . "/test/rcgtest/perl/daqfilters/daqrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/daqrunfile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/daqfilters/Template_Daq_Filters.xml";
$results_file = $rcgtesttmpdir . "/daqdata.xml";
#Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/daqref.data";
$test_data_file = $rcgtesttmpdir . "/daq.data";
my $cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);


#convert .xml files to .data files.
#system ('/usr/bin/xmlconv -m Template_Daq_Filters.xml daqref.data "Result[0]"');
#system ('/usr/bin/xmlconv -m daqdata.xml daq.data "Result[0]"');

#Read xfer function frequency data
(my $testPts,my @freq) = dtReadDataFile($test_data_file,0);

#Read xfer function data for 2x filter
($testPts,my @data2k) = dtReadDataFile($test_data_file,3);
($testPts,my @data2kR) = dtReadDataFile($ref_data_file,3);
my $result2k = "PASS";
my $spec2k = 0.03;
(my $errs, my @diff) = dtCheckData($testPts,$spec2k,@data2k,@data2kR);
print "2K errors = $errs \n";
if($errs) { $result2k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 4x filter
($testPts,my @data4k) = dtReadDataFile($test_data_file,5);
($testPts,my @data4kR) = dtReadDataFile($ref_data_file,5);
my $result4k = "PASS";
my $spec4k = 0.03;
($errs, @diff) = dtCheckData($testPts,$spec4k,@data4k,@data4kR);
print "4K errors = $errs  \n";
if($errs) { $result4k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 8k filter
($testPts,my @data8k) = dtReadDataFile($test_data_file,7);
($testPts,my @data8kR) = dtReadDataFile($ref_data_file,7);
my $result8k = "PASS";
my $spec8k = 0.08;
($errs, @diff) = dtCheckData($testPts,$spec8k,@data8k,@data8kR);
print "8K errors = $errs  \n";
if($errs) { $result8k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 16k filter
($testPts,my @data16k) = dtReadDataFile($test_data_file,9);
($testPts,my @data16kR) = dtReadDataFile($ref_data_file,9);
my $result16k = "PASS";
my $spec16k = 0.2;
($errs, @diff) = dtCheckData($testPts,$spec16k,@data16k,@data16kR);
print "16K errors = $errs  \n";
if($errs) { $result16k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 32k filter
($testPts,my @data32k) = dtReadDataFile($test_data_file,11);
($testPts,my @data32kR) = dtReadDataFile($ref_data_file,11);
my $result32k = "PASS";
my $spec32k = 0.4;
($errs, @diff) = dtCheckData($testPts,$spec32k,@data32k,@data32kR);
print "32K errors = $errs  \n";
if($errs) { $result32k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 64k filter
($testPts,my @data64k) = dtReadDataFile($test_data_file,13);
($testPts,my @data64kR) = dtReadDataFile($ref_data_file,13);
my $result64k = "PASS";
my $spec64k = 0.25;
($errs, @diff) = dtCheckData($testPts,$spec64k,@data64k,@data64kR);
print "64K errors = $errs  \n";
if($errs) { $result64k = "FAIL"; }
if($errs) { $status = -1; }

#Read xfer function data for 128k filter
($testPts,my @data128k) = dtReadDataFile($test_data_file,15);
($testPts,my @data128kR) = dtReadDataFile($ref_data_file,15);
my $result128k = "PASS";
my $spec128k = 0.25;
($errs, @diff) = dtCheckData($testPts,$spec128k,@data128k,@data128kR);
print "128K errors = $errs  \n";
if($errs) { $result128k = "FAIL"; }
if($errs) { $status = -1; }

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


#Make data plots
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/daqfilters/daqFiltTest.plt");
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/daqfilters//daqFiltRef.plt");

# Generate Test Report
$testdatafile = $rcgtestdatadir . "/daqFiltTestData.dox";
open(OUTM,'>', $testdatafile) || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page daqFiltTest daqLib Decimation Filter Test Data
*       \\verbatim
*************************************************************************************************

DAQ DECIMATION FILTER CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************
PURPOSE: Verify the operation of the DAQ decimation filters, as defined in daqLib.c.

OVERVIEW: 
Decimation filters, used to downsample DAQ signals from within a control application from native
code rate to desired recording rate, are defined in daqLib.c. Code in this file is common to all RCG 
applications. In this test, the coefficients for these filters are loaded in test filter modules within
a 16K test application, one filter module for each downsampling filter type. The GDS diag tool is
then used to run a transfer function, and analyze and plot the data.

TEST REQUIREMENTS:
This script (decFiltTest.pl) was designed to run on the LLO DAQ test system. It requires that the x2ats
computer is running the x2atstim16.mdl software. 
There are nine filter modules built into the x2atstim16.mdl for use in running this test:
	- X2ATS_TIM16_T1_DAQ_FILTER_TEST: This filter is used to inject the test signal. Its output is 
	  connected to each of the remaining test filter modules.
	- X2ATS_TIM16_T1_DAQ_FILTER_X2: Loaded with the decimation filter defined for 2X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X4: Loaded with the decimation filter defined for 4X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X8: Loaded with the decimation filter defined for 8X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X16: Loaded with the decimation filter defined for 16X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X32: Loaded with the decimation filter defined for 32X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X64: Loaded with the decimation filter defined for 64X downsampling.
	- X2ATS_TIM16_T1_DAQ_FILTER_X128: Loaded with the decimation filter defined for 128X downsampling.

TEST PROCEDURE:
	- Setup the test filter module switches. 
		- TEST filter input must be turned OFF, as there is normally an ADC signal present.
		- First filter of each of the remaining test filter modules must be turned ON.
	- Run diag tool based on a reference template file (Template_Daq_Filters.xml).
	- Read xfer function data and compare with reference data from template file.
	- Plot reference data and test data.


        \\endverbatim

*       \\verbatim
TEST RESULTS:

 2X Filter	- 	$result2k
 4X Filter	- 	$result4k
 8X Filter	- 	$result8k
16X Filter	- 	$result16k
32X Filter	- 	$result32k
64X Filter	- 	$result64k
128X Filter	- 	$result128k

TEST SUMMARY:  $test_summary

        \\endverbatim

\\image html daqFiltRef.png 
\\image latex daqFiltRef.png 
*       \\verbatim


        \\endverbatim
\\image html daqFiltTest.png 
\\image latex daqFiltTest.png 

\\image html daqFiltTestPhase.png 
\\image latex daqFiltTestPhase.png 
*/
END
close OUTM;
print "TEST COMPLETE ***********************************************************************************\n";

caPut("X2:IOP-SAM_TEST_STATUS","SYSTEM IDLE ");
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "COMPLETION TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";

caPut("X2:IOP-SAM_TEST_STAT_5",1);
if($status < 0) {
    caPut("X2:IOP-SAM_TEST_STAT_5",0);
}

system("cat /tmp/rcgtest/data/daqFiltTestData.dox");

exit($status);

