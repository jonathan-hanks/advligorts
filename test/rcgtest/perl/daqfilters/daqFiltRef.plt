set term png
unset logscale; set logscale x
set title "DAQ (daqLib) Decimation Filter Reference Data"
set key left bottom
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude (db)'
set xrange [10:10000]
#set xtics (10,100,1000,10000)
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100
set grid mxtics ls 101
set output "/tmp/rcgtest/images/daqFiltRef.png"
plot "/tmp/rcgtest/tmp/daqref.data" using 1:4 smooth unique title "2X Dec" , "/tmp/rcgtest/tmp/daqref.data" using 1:6 smooth unique title "4X Dec", "/tmp/rcgtest/tmp/daqref.data" using 1:8 smooth unique title "8X Dec", "/tmp/rcgtest/tmp/daqref.data" using 1:10 smooth unique title "16X Dec","/tmp/rcgtest/tmp/daqref.data" using 1:12 smooth unique title "32X Dec", "/tmp/rcgtest/tmp/daqref.data" using 1:14 smooth unique title "64X Dec", "/tmp/rcgtest/tmp/daqref.data" using 1:16 smooth unique title "128X Dec"
