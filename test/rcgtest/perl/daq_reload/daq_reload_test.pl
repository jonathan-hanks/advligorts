#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq
##
##
use threads;
use threads::shared;
BEGIN {
     $epicsbase = $ENV{'EPICS_BASE'};
         $epicslib = $epicsbase . "/lib/linux-x86_64";
         $epicsperl = $epicsbase . "/lib/perl";
        $rcgtestdir = $ENV{'RCG_TEST_DIR'};
        $rcgdir = $ENV{'RCG_DIR'};
         print "EPICSBASE = $epicsbase \n";
         print "EPICSLIB = $epicslib \n";
         print "EPICSPERL = $epicsperl \n";
         push @INC,"/opt/cdscfg/tst/stddir.pl";
         push @INC,"$epicsperl";
         push @INC,"$epicslib";
        push @INC,"/opt/rtapps/debian10/perlmodules";
    push @INC,"$rcgdir/test/rcgtest/perl";
    push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
    push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
    push @INC,"$rcgdir/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use File::Copy;
use DtTools;

$rcgtestdir = $ENV{'RCG_TEST_DIR'};
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";

my $test_summary = "FAIL";
# Need daqperl module to get data from NDS
require "daq.pm";

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );

sub movefile
{
    my $file2copy = shift;
    my $fileTarget = "/opt/rtcds/tst/x2/chans/daq/X2ATSTIM16.ini";
    copy($file2copy,$fileTarget) or die "copy failed: $!";
}

sub wait_state_change
{
    my $testval = shift;
    my $timer = 0;

    do {
        sleep(1);
        @stateword = caGet("X2:FEC-112_STATE_WORD");
        $timer = $timer + 1;
        $config = ($stateword[0] >> 10);
    }while($config ne $testval);
    print "Config now equals $config at time $timer \n";
    sleep( 10 );
}

sub test_config_state
{
    @stateword = caGet("X2:FEC-112_STATE_WORD");
    my $config = ($stateword[0] >> 10);
    return( $config );
}


# ***********************************************************************
my @daqinfoChans = "X2:FEC-112_CHAN_CNT",
                   "X2:FEC-112_TOTAL",
                   "X2:FEC-112_DAQ_BYTE_COUNT",
                   "X2:FEC-112_EPICS_CHAN_CNT",
                   "X2:DAQ-FB4_X2ATSTIM16_STATUS");
$fileTarget = "/opt/rtcds/tst/x2/chans/daq/X2ATSTIM16.ini";
print "Load original file\n";
movefile("./X2ATSTIM16_orig.ini");
sleep(5);
my $config = test_config_state();
print "Original state = $config \n";
#If a modified config, then load it
if($config)
{
    caPut("X2:DAQ-FEC_112_LOAD_CONFIG",1);
    sleep(12);
}

@stateword = caGet("X2:FEC-112_STATE_WORD");
print "stateword = $stateword[0]\n";
$file2copy = "./X2ATSTIM16_test1.ini";

print "Copy test file\n";
copy($file2copy,$fileTarget) or die "copy failed: $!";
$timer = 0;

do {
    sleep(1);
    @stateword = caGet("X2:FEC-112_STATE_WORD");
    $timer = $timer + 1;
}while(($stateword[0] & 1024) eq 0);

@stateword = caGet("X2:FEC-112_STATE_WORD");
$timer = 0;
sleep(10);

print "Load config\n";
caPut("X2:DAQ-FEC_112_LOAD_CONFIG",1);
do {
    sleep(1);
    @stateword = caGet("X2:FEC-112_STATE_WORD");
    $timer = $timer + 1;
}while(($stateword[0] & 1024) gt 0);

print "stateword = $stateword[0] at $timer\n";

print "Load original file\n";
$file2copy = "./X2ATSTIM16_orig.ini";
copy($file2copy,$fileTarget) or die "copy failed: $!";
sleep(12);
@stateword = caGet("X2:FEC-112_STATE_WORD");

exit ($stateword[0]);
