set term png
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude (db)'
unset logscale; set logscale x
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100
set grid mxtics ls 101

set title "Transfer Function - Pole = 50Hz, Zero = 500Hz, Gain = 0.5"
set output "/tmp/rcgtest/images/ift50p500z.png"
plot "/tmp/rcgtest/tmp/iftref.data" using 1:4 smooth unique title "Ref Data", "/tmp/rcgtest/tmp/ift.data" using 1:4 smooth unique title "Test Data"

set title "Transfer Function - Pole = 100Hz, Zero = 1000Hz, Gain = 1.0"
set output "/tmp/rcgtest/images/ift100p1000z.png"
plot "/tmp/rcgtest/tmp/iftref.data" using 1:6 smooth unique title "Ref Data", "/tmp/rcgtest/tmp/ift.data" using 1:6 smooth unique title "Test Data"

set title "Transfer Function - Pole = 1000Hz, Zero = 50Hz, Gain = 1.0"
set output "/tmp/rcgtest/images/ift1000p50z.png"
plot "/tmp/rcgtest/tmp/iftref.data" using 1:8 smooth unique title "Ref Data", "/tmp/rcgtest/tmp/ift.data" using 1:8 smooth unique title "Test Data"

set title "Transfer Function - Pole = 1000Hz, Zero = 3000Hz, Gain = 1.0"
set output "/tmp/rcgtest/images/ift1000p3000z.png"
plot "/tmp/rcgtest/tmp/iftref.data" using 1:10 smooth unique title "Ref Data", "/tmp/rcgtest/tmp/ift.data" using 1:10 smooth unique title "Test Data"
