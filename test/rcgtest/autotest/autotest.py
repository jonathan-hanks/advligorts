#!/usr/bin/python3
# Run RCG tests automatically and report results.

import os
import os.path as path
import subprocess
from git import Repo
import sys
import time
import history
import shutil
import traceback
from best_commmit import get_deserving_commits

autotest_version = (1,0,0)

base_path = "/var/opt/rcgtest"
source_path = path.join(base_path,"advligorts")
tmp_rcg_path = "/tmp/rcgtest"

def open_log():
    """
    Open the log file for appending
    :return: None
    """
    global base_path
    log_file = path.join(base_path, "last_test.log")
    return open(log_file, "at")

def clear_log():
    """
    Erase existing log file
    :return: None
    """
    log_file = path.join(base_path, "last_test.log")
    with open(log_file, "wt") as f:
        pass

def append_to_log(line):
    """
    Append some text to the log file
    :param line: A string
    :return: None
    """
    with open_log() as f:
        f.write(line + "\n")

def log(line):
    """
    Log a line to the last log file
    :param line: A string.  Newline will be added.
    :return: None
    """
    print(line)
    append_to_log(line)


def error(msg):
    """
    Display and log an error message
    :param msg:
    :return:
    """
    line = "*** ERROR: " + msg
    append_to_log(line)
    sys.stderr.write(line + "\n")


def run_log(*args, **kwargs):
    """
    Log the output of a call to subprocess.run

    :param args: passed to subprocess.run
    :param kwargs: passed to subprocess.run
    :return: return of subprocess.run
    """
    return subprocess.run(stdout=open_log(),stderr=open_log(), *args, **kwargs)


def restart_models(iop_model):
    """
    Stop and start models, using the given iop name.

    :param iop_model: A string
    :return:
    """
    # stop local_dc
    global source_path
    run_log("sudo systemctl stop rts-local_dc".split())

    # stop models
    time.sleep(3)
    log("stopping models")
    stop_script = path.join(source_path, "test/rcgtest/scripts/stoptestmodels.sh")
    run_log([stop_script])
    time.sleep(5)
    run_log(f"rtcds stop {iop_model}".split())

    # start models
    time.sleep(20)
    log("starting models")
    run_log(f"rtcds start {iop_model}".split())
    time.sleep(5)
    start_script = path.join(source_path, "test/rcgtest/scripts/starttestmodels.sh")
    run_log([start_script])

    # start local_dc
    run_log("sudo systemctl start rts-local_dc".split())


def auto_test():
    """
    Run all tests and report results.
    :return: None
    """

    # create base path
    global base_path, source_path
    os.makedirs(base_path, exist_ok=True)

    clear_log()

    # setup history
    current_history = history.History()
    history_path = path.join(base_path, "histories.json")

    log("RCG autotest version" + ".".join([str(s) for s in autotest_version]))

    # determine if scheduled or manual start
    started_by = "scheduler"

    log(f"start type was '{started_by}'")

    # update source

    log(f"updating source directory at {source_path}")

    if path.exists(path.join(source_path, ".git")):
        log(f"path already exists")
        repo = Repo(source_path)
    else:
        log(f"path does not exist.  Cloning the source.")
        repo = Repo.clone_from('https://git.ligo.org/cds/advligorts.git', source_path)
        repo.create_remote('jh','https://git.ligo.org/jonathan-hanks/advligorts.git')
        repo.create_remote('evr','https://git.ligo.org/erik.vonreis/advligorts.git')
    log("fetching from remote")
    if repo.is_dirty():
        raise Exception("Local repo has been modified.")

    # fetch

    if len(repo.remotes) < 1:
        error("There are no remotes in the repo")
    for remote in repo.remotes:
        for fetch_info in remote.fetch():
            log(f"Updated {fetch_info.ref} to {fetch_info.commit}")

    # pick commit

    # commit list ordered by most deserving
    log("finding most deserving commit")
    commit_list = get_deserving_commits(repo)
    most_deserving = ["The top ten most deserving commits are:"]
    for commit in commit_list[:10]:
        most_deserving.append(commit.names[0])
    log("\n\t".join(most_deserving))
    histories = history.load_histories(history_path)

    # find first without a finished history


    test_commit = None
    for commit in commit_list:
        sha_hash = commit.commit.hexsha
        if sha_hash in histories and histories[sha_hash].finished:
            log(f"test for '{commit.names[0]}' already complete. Exiting.")
        else:
            log(f"'{commit.names[0]}' selected for testing")
            test_commit = commit
            break

    if test_commit is None:
        log("No commits to test.")
        sys.exit(0)

    log(f"switching to branch {test_commit.names[0]}")

    repo.head.reference = commit.commit
    repo.head.reset(index=True, working_tree=True)


    # build and install models.
    # set environment variables
    build_path = path.join(base_path, "build")
    if path.exists(build_path):
        shutil.rmtree(build_path)
    os.makedirs(build_path)
    env = os.environ
    env["RTS_VERSION"] = "autotest"
    env["RCG_SRC"] = source_path
    env["RCG_DIR"] = source_path
    env["RCG_BUILD_ROOT"] = build_path
    log("building and installing models")
    build_script = path.join(source_path,"test/rcgtest/scripts/buildtestmodels.sh")
    results = run_log([build_script], env=env)
    if results.returncode != 0:
        raise Exception("Failed build or install")

    restart_models("x2iopsam")

    # run tests
    shutil.rmtree(tmp_rcg_path)
    log("running the tests")
    run_log([path.join(source_path, "test/rcgtest/scripts/rcgtest.pl"), 'all', '-w'], env=env)
    log("tests finished")

    # generate report
    time.sleep(10)
    run_log([f"{source_path}/test/rcgtest/doc/makedoc.sh", source_path], cwd=source_path)

    # archive report
    archive_path = path.join(base_path, f"archive/by_hash/{sha_hash}")
    os.makedirs(archive_path, exist_ok=True)
    archive_fname = path.join(archive_path, "rcgtest.pdf")
    report_path = path.join(source_path, "test/rcgtest/doc/rcgtest.pdf")
    shutil.copy(report_path, archive_fname)
    # create symbolic links to archive
    for name in test_commit.names:
        dirs = name.split('/')
        linkname = dirs[-1]
        name_path = path.join(base_path, "archive", *dirs[:-1])
        os.makedirs(name_path, exist_ok=True)
        name_path = path.join(name_path, linkname)
        if path.exists(name_path):
            os.remove(name_path)
        os.symlink(archive_path, name_path, target_is_directory=True);
    log(f"report archived to {archive_fname}")


    # mail report

    # write out history
    log("recording history")
    current_history.finished = True
    history.add_history(history_path, sha_hash, current_history)

    # archive log
    archive_log = path.join(archive_path, "output.log")
    log_path = path.join(base_path, "last_test.log")
    shutil.copy(log_path, archive_log)


if __name__ == "__main__":
    pass
    try:
        auto_test()
    except Exception as e:
        error(str(e))
        tb = traceback.format_exc()
        log(str(tb))
        sys.exit(1)
