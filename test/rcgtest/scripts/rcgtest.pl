#!/usr/bin/perl -w 
##
##

use Switch;

sub daqfilter {
    print "Starting daqfilter test\n";
    system("$rcgdir/perl/daqfilters/daqFiltTest.pl $rcgtestdir");
}

sub decfilter {
    print "Starting decfilter test\n";
    system("$rcgdir/perl/decfilters/decFiltTest.pl $rcgtestdir");
}

sub dackilltimed {
	print "Starting DACKILL TIMED test\n";
    system("$rcgdir/python/dackill/dackilltimed.py");
}

sub duotone {
	print "Starting duotone test\n";
	system("$rcgdir/perl/duotone/duotoneTest.pl $rcgtestdir");
}

sub matrix {
	print "Starting matrix test\n";
    system("$rcgdir/perl/matrix.pl $rcgtestdir");
}

sub simparts {
	print "Starting Simlink parts test\n";
	system("$rcgdir/perl/simPartsTest.pl $rcgtestdir")
}

sub osc {
	print "Starting oscillator part test\n";
	system("$rcgdir/perl/osc/oscTest.pl $rcgtestdir");
}

sub fmc {
	print "Starting filter module w/control test\n";
	system("$rcgdir/perl/fmc/fmctest.pl");
}

sub fmc2 {
	print "Starting filter module w/control 2 test\n";
	system("$rcgdir/perl/fmc2/fmc2test.pl");
}

sub sfm {
	print "Starting standard IIR filter test\n";
	system("$rcgdir/perl/sfm/sfmTest.pl $rcgtestdir");
}

sub inputfilter {
	print "Starting input filter test\n";
	system("$rcgdir/perl/inputfilter/inputfilterTest.pl $rcgtestdir");
}

sub ioptest {
	print "Starting IOP test\n";
	system("$rcgdir/perl/rcgcore/ioptest.pl $rcgtestdir");
}

sub dac {
	print "Starting DAC test\n";
	system("$rcgdir/python/dactest/dactest.py");
}

sub epics_in_ctl {
	print "Starting EPICS INPUT W/Control test\n";
	system("$rcgdir/python/epicsparts/epicsInCtrl.py");
}

sub exctp {
	print "Starting EXCTP test\n";
	system("$rcgdir/python/exctp/extpTest.py");
}

sub rampmuxmatrix {
	print "Starting RampMuxMatrix test\n";
	system("$rcgdir/python/rampmuxmatrix/rampmuxmatrix.py");
}

sub phase {
	print "Starting Phase part test\n";
	system("$rcgdir/python/phase/phase.py");
}

sub binaryIO {
	print "Starting binary IO test\n";
	system("$rcgdir/python/binaryIO/biotest.py");
}

# Test groups

sub testIO {
	ioptest();

	sleep(30);

	binaryIO();
	dac();
	duotone();
}

sub matrixtest {
	matrix();
	rampmuxmatrix();
}

sub testfilters() {
	decfilter();
	sleep(5);
	daqfilter();
	sleep(5);
	fmc2();
	sleep(5);
	fmc();
	sleep(5);
	inputfilter();
	sleep(5);
	sfm();
}

sub miscparts {
	dackilltimed();
	osc();
	phase();
	exctp();
	epics_in_ctl();
	simparts();
}

sub all {
	testIO();
	sleep(5);
	testfilters();
	sleep(5);
	matrixtest();
	sleep(5);
	miscparts();
}

my $test2run = $ARGV[0];

# when true, fork a process to handle
# tests while the main process quits
my $fork_flag=1;

if (@ARGV > 1)
{
	my $flag = $ARGV[1];

	# wait for tests to finish (dont fork).  Scripts want this, but medms don't
	if($flag eq "-w")
	{
		$fork_flag=0;
	}
}

$rcgdir = $ENV{'RCG_DIR'};
$rcgdir .="/test/rcgtest";

$rcgtestdir = "/tmp/rcgtest";
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";
$rcgtestdocdir = $rcgtestdir . "/doc";
$rcgtestlatexdir = $rcgtestdocdir . "/latex";

#Create test data directories if they do not exist
unless( -e $rcgtestdir or mkdir $rcgtestdir) {
    die "unable to create test results directories\n";
}
unless( -e $rcgtestdatadir or mkdir $rcgtestdatadir) {
    die "unable to create test data directories\n";
}
unless( -e $rcgtestimagesdir or mkdir $rcgtestimagesdir) {
    die "unable to create test images directories\n";
}
unless( -e $rcgtesttmpdir or mkdir $rcgtesttmpdir) {
    die "unable to create test tmp directories\n";
}
unless( -e $rcgtestdocdir or mkdir $rcgtestdocdir) {
    die "unable to create test doc directories\n";
}
unless( -e $rcgtestlatexdir or mkdir $rcgtestlatexdir) {
    die "unable to create test doc directories\n";
}

if ($fork_flag)
{
    # run in the background
    my $pid = fork;
    exit if $pid;
}

#Determine which test to run
switch($test2run)
{

    case "daqfilter" {
        daqfilter();
    }
    case "decfilter" {
        decfilter();
    }
    case "duotone" {
        duotone();
    }
    case "matrix" {
        matrix();
    }
    case "simparts" {
        simparts();
    }
    case "osc" {
        osc();
    }
    case "fmc" {
        fmc();
    }
    case "fmc2" {
		fmc2();
    }
    case "sfm" {
		sfm();
    }
    case "inputfilter" {
		inputfilter();
    }
    case "ioptest" {
		ioptest();
    }
    case "dac" {
		dac();
    }
    case "epics_in_ctl" {
		epics_in_ctl();
    }
    case "exctp" {
		exctp();
    }
    case "dackilltimed" {
    	dackilltimed();
    }
    case "rampmuxmatrix" {
		rampmuxmatrix();
    }
    case "phase" {
		phase();
    }
    case "binaryIO" {
		binaryIO();
    }
    case "testIO" {
		testIO();
    }
    case "matrixtest" {
		matrixtest();
    }
    case "testfilters" {
		testfilters();
    }
    case "miscparts" {
		miscparts();
    }
    case "all" {
    	all();
    }
    else {
        print "Unknown test\n";
    }
}

